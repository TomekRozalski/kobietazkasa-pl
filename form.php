<?php

$value = $_GET['val'];
$fee = $_GET['fee'];
$rrso = $_GET['rrso'];
$total = $value + $fee;

$html_file = file_get_contents('themes/kobieta-z-klasa/views/form/content.html');

$html_file = str_replace('#LOAN.SUM#', $value, $html_file);
$html_file = str_replace('#LOAN.FEE#', $fee, $html_file);
$html_file = str_replace('#LOAN.TOTAL#', $total, $html_file);
$html_file = str_replace('#LOAN.RRSO#', $rrso, $html_file);

$html = $html_file;

//==============================================================
//==============================================================
//==============================================================

include("themes/kobieta-z-klasa/libraries/mpdf60/mpdf.php");
$mpdf=new mPDF('utf-8');

// LOAD a stylesheet
$stylesheet = file_get_contents('themes/kobieta-z-klasa/views/form/style.css');
$mpdf->WriteHTML($stylesheet,1);    // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->shrink_tables_to_fit=1;
$mpdf->WriteHTML($html);
$mpdf->Output('formularz.pdf', 'D');
exit;

//==============================================================
//==============================================================
//==============================================================