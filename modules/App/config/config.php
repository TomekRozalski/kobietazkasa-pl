<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module']['app'] = array
(
    'module' => "App",
    'name' => "App Module",
    'description' => "Application specific.",
    'author' => "Michał Zagdan",
    'version' => "1.1",

    // 'uri' should be the module's folder in lowercase.
    // From 1.0.3, it is not mandatory to set 'uri'.
    'uri' => 'app',
    'has_admin'=> TRUE,
    'has_frontend'=> TRUE,
);

return $config['module']['app'];
