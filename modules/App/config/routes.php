<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "app";
$route['(.*)'] = "app/$1";
$route[''] = 'app/index';
