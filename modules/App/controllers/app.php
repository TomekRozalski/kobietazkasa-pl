<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include dirname(__FILE__).'/../libraries/CashbillClient.php';

class App extends My_Controller
{
    public function __construct()
    {
        if (ENVIRONMENT == 'production') {
            $this->wsdl = 'https://pay.cashbill.pl/ws/soap/PaymentService?wsdl';
        } else {
            $this->wsdl = 'https://pay.cashbill.pl/testws/soap/PaymentService?wsdl';
        }

        parent::__construct();
    }

    function index()
    {
        print "Demo module default controller output";
    }

    function cashbill()
    {
        $this->load->helper('url');
        $this->session->set_userdata('iban', $this->input->post('iban'));

        $client = new CashbillClient($this->wsdl);

        try {
            $ret = $client->newPayment(array(
                'title' => 'Płatność weryfikacyjna',
                'amount' => array(
                    'value' => 0.01,
                    'currencyCode' => 'PLN'
                ),
                'returnUrl' => site_url('/cashbill/weryfikacja-nie-powiodla-sie'),
                'negativeReturnUrl' => site_url('/cashbill/konto-zweryfikowane'),
                'personalData' => array(
                    'firstName' => $this->session->userdata('first_name'),
                    'surame'    => $this->session->userdata('last_name'),
                    'email'     => $this->session->userdata('email'),
                ),
                'verificationData' => array(
                    'name'      => $this->session->userdata('first_name').' '.$this->session->userdata('last_name'),
                    'street'    => $this->session->userdata('address').' '.$this->session->userdata('house_number'),
                    'city'      => $this->session->userdata('city'),
                    'iban'      => $this->session->userdata('iban'),
                ),
                'confirmVerificationData' => 1,
            ));
        } catch (SoapFault $e) {
            log_message('error', 'SOAP ERROR: '.$e->getMessage());
            print 'Blad weryfikacji konta';
            exit;
        }

        $this->session->set_userdata('cashbill_order_id', $ret->return->id);
        $redirectUrl = $ret->return->redirectUrl;

        redirect($redirectUrl);
    }
    
    /**
     * Function to fulfill Cashbill requirements
     */
    public function notify()
    {
        echo 'OK';
        return;
    }
}
