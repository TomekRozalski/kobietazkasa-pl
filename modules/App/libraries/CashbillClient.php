<?php

class CashbillClient extends SoapClient
{
    private $shopId = "kobietazkasa.pl";
    private $secretPhrase = "daf9e6b845bc36f46cdeac9f44f790a2";

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $sec = <<<EOS
<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
<wsse:UsernameToken wsu:Id="UsernameToken-1">
<wsse:Username>{$this->shopId}</wsse:Username>
<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">{$this->secretPhrase}</wsse:Password>
</wsse:UsernameToken>
</wsse:Security>
EOS;

        $xml = new DOMDocument();
        $xml->loadXML($request);
        $header = $xml->createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "Header");
        $node = $xml->createDocumentFragment();
        $node->appendXML($sec);
        $header->appendChild($node);
        $xml->firstChild->insertBefore($header, $xml->firstChild->firstChild);
        $request = $xml->saveXML();

        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }
}
