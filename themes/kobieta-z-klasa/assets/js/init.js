var fee = new Array(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 20.5, 21, 21.5, 22, 22.5, 23, 23.5, 24, 24.5, 25, 25.3, 25.6, 25.9, 26.2, 26.5, 26.8);

function setValues() {

	v_value = parseInt($('#kwota').val());
	v_curr = parseInt($('#czas').val());
	today = new Date();

	//if(v_value <= 500 && v_curr <= 7) {

	//	rrso = 0;
	//	finalFee = 0;

	ContractSettings = {
		prices:     [{"percent":720.00,"sum":null,"period":5,"extension_percent":648.00},{"percent":660.00,"sum":null,"period":6,"extension_percent":600.00},{"percent":617.14,"sum":null,"period":7,"extension_percent":565.71},{"percent":585.00,"sum":null,"period":8,"extension_percent":540.00},{"percent":560.00,"sum":null,"period":9,"extension_percent":520.00},{"percent":540.00,"sum":null,"period":10,"extension_percent":504.00},{"percent":523.64,"sum":null,"period":11,"extension_percent":490.91},{"percent":510.00,"sum":null,"period":12,"extension_percent":480.00},{"percent":498.46,"sum":null,"period":13,"extension_percent":470.77},{"percent":488.57,"sum":null,"period":14,"extension_percent":462.86},{"percent":480.00,"sum":null,"period":15,"extension_percent":456.00},{"percent":461.25,"sum":null,"period":16,"extension_percent":438.75},{"percent":444.71,"sum":null,"period":17,"extension_percent":423.53},{"percent":430.00,"sum":null,"period":18,"extension_percent":410.00},{"percent":416.84,"sum":null,"period":19,"extension_percent":397.89},{"percent":405.00,"sum":null,"period":20,"extension_percent":387.00},{"percent":394.29,"sum":null,"period":21,"extension_percent":377.15},{"percent":384.55,"sum":null,"period":22,"extension_percent":368.19},{"percent":375.65,"sum":null,"period":23,"extension_percent":360.00},{"percent":367.50,"sum":null,"period":24,"extension_percent":352.50},{"percent":360.00,"sum":null,"period":25,"extension_percent":345.60},{"percent":350.31,"sum":null,"period":26,"extension_percent":336.46},{"percent":341.33,"sum":null,"period":27,"extension_percent":328.00},{"percent":333.00,"sum":null,"period":28,"extension_percent":320.14},{"percent":325.24,"sum":null,"period":29,"extension_percent":312.83},{"percent":318.00,"sum":null,"period":30,"extension_percent":306.00},{"percent":311.23,"sum":null,"period":31,"extension_percent":299.62}]
	};


	//} else {

		//rrso = ((Math.pow((1+(fee[v_curr - 5]/100)), (365/v_curr)) - 1)*100).toFixed(1);
		rrso = ((Math.pow(((0.01 + v_value * (1 + (fee[v_curr - 5]/100))) / v_value), (365/v_curr)) - 1 ) * 100).toFixed(1);
		finalFee = (((fee[v_curr - 5]/100) / v_curr) * v_curr * (v_value + 1)).toFixed(0);

	//}

	total = parseInt(finalFee) + parseInt(v_value);
	today.setDate(today.getDate() + v_curr);

	$('.rrso').text(rrso);
	$('.fee').text(finalFee);
	$('.total').text(total);
	$('.l_value').text(v_value);
	$('.to').text(today.getDate()+'.'+(today.getMonth()+1)+'.'+today.getFullYear());

}

function validatepostalcode(code)
{
  var regex = /^[0-9]{2}\-?[0-9]{3}$/;
  return regex.test(code);
}

function validatepesel(pesel) {
    var reg = /^[0-9]{11}$/;
    if(reg.test(pesel) == false) {
    return false;}
    else
    {
        var dig = (""+pesel).split("");
        var kontrola = (1*parseInt(dig[0]) + 3*parseInt(dig[1]) + 7*parseInt(dig[2]) + 9*parseInt(dig[3]) + 1*parseInt(dig[4]) + 3*parseInt(dig[5]) + 7*parseInt(dig[6]) + 9*parseInt(dig[7]) + 1*parseInt(dig[8]) + 3*parseInt(dig[9]))%10;
        if(kontrola==0) kontrola = 10;
        kontrola = 10 - kontrola;
        if(parseInt(dig[10])==kontrola)
        return true;
        else
        return false;
    }

}

function validatebankaccount(nrb) {
    nrb = nrb.replace(/[^0-9]+/g,'');
    var Wagi = [1,10,3,30,9,90,27,76,81,34,49,5,50,15,53,45,62,38,89,17, 73,51,25,56,75,71,31,19,93,57];

    if(nrb.length == 26) {
        nrb = nrb + "2521";
        nrb = nrb.substr(2) + nrb.substr(0,2);
        var Z =0;
        for (var i=0;i < 30; i++) {
            Z += nrb[29-i] * Wagi[i];
        }
        return Z % 97 == 1;
    } else {
        return false;
    }
}

function validateid(nd) {

	if(nd == null) nd = 0;

		return eval(nd.length===9 && parseInt(nd[3]) === eval(nd.split("").map(function(v, i) {return(v.charCodeAt() * [7,3,1,0,7,3,1,7,3][i]) - [385,165,55,0,336,144,48,336,144][i];}).join('+'))%10);

}

$(function(){

	setValues();

	$('select').selectBox();

	$('form').submit(function(){

		$('.error').each(function() {

			$(this).removeClass('error');

		});

		var status = true,
			hand = $(this),
			emailTest = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

		hand.find('input[type=text].req:not(.email):not(.code):visible, textarea.req:visible, input[type=password].req:visible').each(function(){
			if ($(this).val() == '' || $(this).val().length < 2) { $(this).addClass('error'); status = false;  }
		});
		hand.find('input[type=text].req.code:visible').each(function(){
			if ($(this).val() == '') { $(this).addClass('error'); status = false;  }
		});
		hand.find('input[type=text].email.req:visible').each(function(){
			if (!emailTest.test($(this).val())) { $(this).addClass('error'); status = false;  }
		});
		hand.find('input[type=checkbox].req').each(function(){
				if ($(this).is(':not(:checked)')) { $(this).parent().addClass('error'); status = false;  }
		});
		hand.find('select.req').each(function(){
			hand2 = $(this);
			if(hand2.next('.selectBox').is(':visible')) {
				if (hand2.val() == '') { hand2.next().addClass('error'); status = false;  }
			}
		});
		hand.find('.number:visible').each(function(){
			if ($(this).val() > 999999999 || $(this).val() < 100000000 || isNaN($(this).val())) { $(this).addClass('error'); status = false;  }
		});
		hand.find('.kod1:visible').each(function(){
 		//	if ($(this).val().length < 5) { $(this).addClass('error'); status = false;  }
		if(validatepostalcode($(this).val()) === false) { $(this).addClass('error'); status = false; }
 		});

		hand.find('.kod2:visible').each(function(){
 			if ($(this).val().length < 1 || isNaN($(this).val())) { $(this).addClass('error'); status = false;  }
 		});

		hand.find('.kod3:visible').each(function(){
 			if ($(this).val().length < 1) { $(this).addClass('error'); status = false;  }
 		});

		if(validatepesel(hand.find('.pesel').val()) === false && $('.pesel').size() > 0) { $('.pesel').addClass('error'); status = false; }

		if(validateid($('.idcard').val()) === false && $('.idcard').size() > 0) { $('.idcard').addClass('error'); status = false; }

        if(validatebankaccount(hand.find('#bank_account').val()) === false && $('#bank_account').size() > 0) { $('#bank_account').addClass('error'); status = false; }

		return status;
	});
	$('input, textarea').keyup(function(){ $(this).removeClass('error'); });
	$('select').change(function(){ $(this).next().removeClass('error'); });
	$('input[type=checkbox]').change(function(){ $(this).parent().removeClass('error'); });

	$('#cz_lives_at_reqistred_address').change(function() {
		if($(this).is(':checked') === true) {
			$('.h_s_1').hide();
		} else {
			$('.h_s_1').show();
		}
	});

	$('select#customer_occupation').change(function(){

		opt = $(this).val();
		$('.h_r_1, .h_r_2, .h_r_3, .h_r_4, .h_r_5, .h_r_6, .h_r_7, .h_r_8, .h_r_9').hide();

		if(opt == 'employed_indefinite_period' || opt == 'employed_specified_period' || opt == 'written_contract_or_order') {
			$('.h_r_1, .h_r_2, .h_r_3, .h_r_4, .h_r_5, .h_r_6').show();
		} else if(opt == 'economic_activity') {
			$('label[for="customer_employer_name"]').text('Nazwa firmy');
			$('.h_r_1, .h_r_3, .h_r_4, .h_r_5, .h_r_6, .h_r_7, .h_r_10').show();
		} else if(opt == 'student') {
			$('.h_r_1, .h_r_8, .h_r_9').show();
		} else if(opt == 'pensioner1' || opt == 'pensioner2' || opt == 'other') {
			$('.h_r_1, .h_r_4, .h_r_6').show();
		} else if(opt == 'unemployed'){
			$('.h_r_1').show();
		}

	});

	$('label.chck').each(function(){
		additionalClass = $(this).attr('for');
			if ($(this).find('input:checked').length > 0) {
					$(this).addClass('on');
					$(this).prepend('<span class="tick on ' + additionalClass + '"></span>');
			} else {
					$(this).prepend('<span class="tick ' + additionalClass + '"></span>');
			}
	})
	$('label.chck input').change(function(){
			$(this).parent().find('span.tick').toggleClass('on');
	});

	$('.faq div.faq_column ul li h5').click(function(){
		$(this).parent().toggleClass('open');
	});

	$('.faq div.faq_column ul li h5').click(function(){
		$('.faq div.faq_column ul li.open').not($(this).parent()).removeClass('open');
		$(this).toggleClass('open');
	});

	var ustawKwote = function(action) {
		var v_min = 100,
			v_max = 1500,
			step = 50,
			input = $('#kwota'),
			v_curr = parseInt($('#kwota').val());

		if (action == 'plus') {
			v_curr + step <= v_max ? input.val(v_curr+step).trigger('change') : false;
		} else if (action == 'minus') {
			v_curr - step >= v_min ? input.val(v_curr-step).trigger('change') : false;
		}
	}

	// Link do formularza informacyjnego
	$('#formularz').attr('href', 'form.php?val='+ v_value +'&fee='+ finalFee +'&rrso='+ rrso);

	$('#kwota').change(function(){
		$('.kolo1 div strong').text($(this).val());
		setValues();
		$('#formularz').attr('href', 'form.php?val='+ v_value +'&fee='+ finalFee +'&rrso='+ rrso);
	});

	$('form:not(.locked) .kolo1 .plus').click(function(e){
		e.preventDefault();
		ustawKwote('plus');
	});
	$('form:not(.locked) .kolo1 .minus').click(function(e){
		e.preventDefault();
		ustawKwote('minus');
	});

	var ustawCzas = function(action) {
		var v_min = 7,
			v_max = 31,
			step = 1,
			input = $('#czas'),
			v_curr = parseInt($('#czas').val());

		if (action == 'plus') {
			v_curr + step <= v_max ? input.val(v_curr+step).trigger('change') : false;
		} else if (action == 'minus') {
			v_curr - step >= v_min ? input.val(v_curr-step).trigger('change') : false;
		}

	}

	$('#czas').change(function(){
		$('.kolo2 div strong').text($(this).val());
		setValues();
		$('#formularz').attr('href', 'form.php?val='+ v_value +'&fee='+ finalFee +'&rrso='+ rrso);
	})

	$('form:not(.locked) .kolo2 .plus').click(function(e){
		e.preventDefault();
		ustawCzas('plus');
	});
	$('form:not(.locked) .kolo2 .minus').click(function(e){
		e.preventDefault();
		ustawCzas('minus');
	});

	$('.pwd, .re_pwd').keyup(function(){
		var pwd = $('.pwd'),
			re_pwd = $('.re_pwd');
		if (pwd.val() == re_pwd.val() && pwd.val().length >= 6) {
			re_pwd.removeClass('wrong').addClass('ok')
		} else {
			re_pwd.removeClass('ok').addClass('wrong');
		}
	});

	$('#bank').change(function() {

		$('.bank').hide();

		$('.bank' + $('#bank option:checked').val()).show();

	});

});
