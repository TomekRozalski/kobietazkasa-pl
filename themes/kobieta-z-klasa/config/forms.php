<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Theme Forms configuration
  |--------------------------------------------------------------------------
  |
  | This forms config array will be merged with /application/config/forms.php
  | You can overwrite standard forms definition by creating your own definition
  | for the form you wish to overwrite.
  |
 */
$config['forms'] = array
    (
		'kalkulator' => array
		(
			'process' => 'TagManager_Pozyczkomat::process_data',
		),

		'rejestracja' => array
        (
        'process' => 'TagManager_Pozyczkomat::process_data',
        'fields' => array
            (
            'first_name' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Imię',
            ),
            'last_name' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Nazwisko',
            ),
            'birth_date_3i' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Dzień',
            ),
            'birth_date_2i' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Miesiąc',
            ),
            'birth_date_1i' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Rok',
            ),
            'email' => array(
                'rules' => 'trim|required|valid_email|xss_clean',
                'label' => 'Adres e-mail',
            ),
            'phone' => array(
                'rules' => 'trim|required|numeric|xss_clean|min_length[9]|max_length[9]',
                'label' => 'Telefon komórkowy',
            ),
            'password' => array(
                'rules' => 'trim|required|matches[password2]|xss_clean|min_length[6]|max_length[20]',
                'label' => 'Hasło',
            ),
            'password2' => array(
                'rules' => 'trim|required|matches[password]|xss_clean|min_length[6]|max_length[20]',
                'label' => 'Powtórz hasło',
            ),
            'agree_electronic_services' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Zgoda na wysyłkę elektroniczną',
            ),
            'agree_personal_data_protection' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Ochrona danych osobowych',
            ),
        ),
	),
	    'dane' => array
	        (
	        'process' => 'TagManager_Pozyczkomat::process_data',
			'messages' => array(
			            'success' => 'form_message_success',
						            'error' => 'form_alert_error_message',
			),
	        'fields' => array
	            (
	            'personal_id' => array(
	                'rules' => 'trim|required|numeric|exact_length[11]|xss_clean',
	                'label' => 'PESEL',
	            ),
	            'id_card_number' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Numer dowodu osobistego',
	            ),
                'bank_account' => array(
                    'rules' => 'trim|required|xss_clean',
                    'label' => 'Numer rachunku bankowego',
                ),
	            //'mothers_maiden_name' => array(
	            //    'rules' => 'trim|required|xss_clean',
	            //    'label' => 'Nazwisko panieńskie matki',
	            //),
	            'education' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Wykształcenie',
	            ),
	            'marital_status' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Stan cywilny',
	            ),
	            //'dependant_count' => array(
	            //    'rules' => 'trim|required|xss_clean',
	            //    'label' => 'Liczba osób na utrzymaniu',
	            //),
	            'car' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Czy posiadasz samochód',
	            ),
	            'housing_type' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Status zamieszkania',
	            ),
	            'housing_type_lived_years' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Jak długo mieszkasz pod obecnym adresem?',
	            ),
	            'housing_type_lived_months' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Jak długo mieszkasz pod obecnym adresem?',
	            ),
	            'address' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Ulica',
	            ),
	            'house_number' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Numer domu',
	            ),
	            'city' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Miasto',
	            ),
	            'postal_index' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Kod pocztowy',
	            ),
	            'occupation' => array(
	                'rules' => 'trim|required|xss_clean',
	                'label' => 'Forma zatrudnienia',
	            ),
	        ),
    ),
    // Contact form
    'contact' => array
        (
        // The method which will process the form
        // The function name has no importance, it must only be in the declared Tagmanager class
        // and be "public static"
        'process' => 'TagManager_Contact::process_data',
        // Redirection after process. Can be 'home' or 'referer' for the $_SERVER['HTTP_REFERER'] value.
        // If not set, doesn't redirect
        'redirect' => 'referer',
        // Messages Language index, as set in language/xx/form_lang.php
        'messages' => array(
            'success' => 'form_alert_success_message',
            'error' => 'form_alert_error_message',
        ),
        'emails' => array
            (
            // To Site Administrator
            array
                (
                // Send the mail to the address filled in in the 'email' input of the form
                // Values can be :
                // - One plain Email address : my.name@mydomain.com
                // - 'form' to send it to the email of the form data
                // - 'site' to send it to the Email set in Ionize under Settings > Advanced > Email > Website
                // - 'contact' to send it to the Email set in Ionize under Settings > Advanced > Email > Contact
                // - 'info' to send it to the Email set in Ionize under Settings > Advanced > Email > Info
                // - 'technical' to send it to the Email set in Ionize under Settings > Advanced > Email > Info
                'email' => 'contact',
                // Translation item index
                'subject' => 'mail_website_contact_subject',
                // Used view : Located in /themes/your_theme/mail/contact.php
                'view' => 'mail/contact',
            ),
            // Send to user
          //  array
          //      (
          //      'email' => 'form',
           //     'subject' => 'mail_user_contact_subject',
         //       'view' => 'mail/contact',
         //   ),
        ),
        // Form definition: fields and rules
        'fields' => array
            (
            'name' => array
                (
                // CI validation rules
                'rules' => 'trim|required|xss_clean',
                // Label translated index, as set in language/xx/form_lang.php
                // Will be used to display the label name in error messages
                'label' => 'Imię i nazwisko',
            ),
            'email' => array(
                'rules' => 'trim|required|valid_email|xss_clean',
                'label' => 'Email',
            ),
            'subject' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Temat',
            ),
            'message' => array(
                'rules' => 'trim|required|xss_clean',
                'label' => 'Wiadomość',
            )
        )
    ),
);
