<?php


class TagManager_Pozyczkomat extends TagManager
{

	public static function process_data(FTL_Binding $tag)
	{

		$form_name = self::$ci->input->post('form');

		if (TagManager_Form::validate($form_name))
		{

			if($form_name == 'kalkulator') {

				self::$ci->session->set_userdata( 'period', self::$ci->input->post('period') );
				self::$ci->session->set_userdata( 'value', self::$ci->input->post('value'));
				if(isset($_COOKIE['aff'])) {
					self::$ci->session->set_userdata( 'aff', $_COOKIE['aff']);
				} else {
					self::$ci->session->set_userdata( 'aff', 'no_aff');
				}

				if(isset($_COOKIE['prod_id'])) {
					self::$ci->session->set_userdata( 'prod_id', $_COOKIE['prod_id']);
				}

				if(isset($_COOKIE['bankier_id'])) {
					self::$ci->session->set_userdata( 'bankier_id', $_COOKIE['bankier_id']);
				}
				
				if(isset($_COOKIE['transaction_id'])) {
					self::$ci->session->set_userdata( 'transaction_id', $_COOKIE['transaction_id']);
				}

				redirect(base_url().'rejestracja');

			}

			if($form_name == 'rejestracja') {

				self::$ci->session->set_userdata( 'first_name', self::$ci->input->post('first_name') );
				self::$ci->session->set_userdata( 'last_name', self::$ci->input->post('last_name') );
				self::$ci->session->set_userdata( 'birth_date', self::$ci->input->post('birth_date_1i') . '-' . 				self::$ci->input->post('birth_date_2i') . '-' .self::$ci->input->post('birth_date_3i') );
				self::$ci->session->set_userdata( 'email', self::$ci->input->post('email') );
				self::$ci->session->set_userdata( 'phone', self::$ci->input->post('phone'));
				self::$ci->session->set_userdata( 'password', self::$ci->input->post('password') );
				self::$ci->session->set_userdata( 'password_confirmation', self::$ci->input->post('password2') );
				self::$ci->session->set_userdata( 'agree_electronic_services', self::$ci->input->post('agree_electronic_services') );
				self::$ci->session->set_userdata( 'agree_personal_data_protection', self::$ci->input->post('agree_personal_data_protection') );

				redirect(base_url().'dane');

			}

			if($form_name == 'dane') {

				if(self::$ci->input->post('remuneration_deadline1i') > 0) {

				$remu_data = self::$ci->input->post('remuneration_deadline1i') . '-' . self::$ci->input->post('remuneration_deadline2i') . '-' .self::$ci->input->post('remuneration_deadline3i');

			} else {

				$remu_data = date('Y-m-d', strtotime("+1 month"));

			}

				if(self::$ci->input->post('business_registration_date1i') > 0) {

					$bus_data = self::$ci->input->post('business_registration_date1i') . '-' . self::$ci->input->post('business_registration_date2i') . '-' . self::$ci->input->post('business_registration_date3i');

				} else {
					$bus_data = date('Y-m-d', strtotime("-1 month"));;

				}
				
				if(self::$ci->input->post( 'current_employment_years' ) == 0 && self::$ci->input->post( 'current_employment_months' ) == 0) {
					$current_employment_years = 0;
					$current_employment_months = 1;
				} else {
					$current_employment_years = self::$ci->input->post( 'current_employment_years' );
					$current_employment_months = self::$ci->input->post( 'current_employment_months' );
				}



				$data = array(
					'customer' => array(
						'first_name' => self::$ci->session->userdata( 'first_name' ),
						'last_name' => self::$ci->session->userdata( 'last_name' ),
						'birth_date' => self::$ci->session->userdata( 'birth_date' ),
						'phone' => self::$ci->session->userdata( 'phone' ),
						'email' => self::$ci->session->userdata( 'email' ),
						'password' => self::$ci->session->userdata( 'password' ),
						'password_confirmation' => self::$ci->session->userdata( 'password_confirmation' ),
						'email_confirmation' => self::$ci->session->userdata( 'email' ),
						'agree_electronic_services' => self::$ci->session->userdata( 'agree_electronic_services' ),
						'agree_personal_data_protection' => self::$ci->session->userdata( 'agree_personal_data_protection' ),

						'city' => self::$ci->input->post( 'city' ),
						'address' => self::$ci->input->post( 'address' ),
						'house_number' => str_replace('\\', '/',self::$ci->input->post( 'house_number' )),
						'postal_index' => str_replace('-', '', self::$ci->input->post( 'postal_index' )),
						//'mothers_maiden_name' => self::$ci->input->post( 'mothers_maiden_name' ),
						'education' => self::$ci->input->post( 'education' ),
						'marital_status' => self::$ci->input->post( 'marital_status' ),
						//'dependant_count' => self::$ci->input->post( 'dependant_count' ),
						'car' => self::$ci->input->post( 'car' ),
						'housing_type' => self::$ci->input->post( 'housing_type' ),
						'occupation' => self::$ci->input->post( 'occupation' ),
						'id_card_number' => self::$ci->input->post( 'id_card_number' ),
						'personal_id' => self::$ci->input->post( 'personal_id' ),
						'current_employment_years' => $current_employment_years,
						'current_employment_months' => $current_employment_months,
						'remuneration_deadline' => $remu_data,
						'salary_to_personal_account' => self::$ci->input->post( 'customer_salary_to_personal_account' ),
						'employer_name' => self::$ci->input->post( 'employer_name' ),
						'employer_work_city' => self::$ci->input->post( 'employer_work_city' ),
						'neto_income' => round(self::$ci->input->post( 'neto_income' )),
						'university_city' => self::$ci->input->post( 'university_city' ),
						'university_name' => self::$ci->input->post( 'university_name' ),
						'business_registration_date' => $bus_data,
					),
					'loan' => array(
						'loan_period' => self::$ci->session->userdata( 'period' ),
						'loan_sum' => self::$ci->session->userdata( 'value' ),
					),
					'conditions' => array(
						'accept_loan_contract_and_information_form' => '1',
					),
					'api_key' => '065e38a9c5076e13816ff7a97c193962',
					//'api_key' => '8567a6bc228391e8beb60c06f8675ef3' ,// test
					'affiliate_identifier' => 'kobietazkasa_' . self::$ci->session->userdata( 'aff' ),

				);

				$content = json_encode($data);

				//$url = 'http://pl-api:U2e20LC5@staging-pl.friendly-finance.eu/api/pl/v2/signup'; // test
				$url = 'https://www.pozyczkomat.pl/api/pl/v2/signup';

				$curl = curl_init($url);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPHEADER,
				array('Content-type: application/json'));
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

				$result     = curl_exec($curl);
				$response   = json_decode($result);

				curl_close($curl);

				if($response->status == 'ACCEPTED'){

					//$url = 'http://www.google-analytics.com/collect?v=1&tid=UA-43038387-3&cid=' . $response->customer_id . '&t=event&ec=New_loan&ea='. self::$ci->session->userdata( 'aff' ) . '&el='. $response->customer_id . '&ni=1';
					//$curl = curl_init($url);
					//curl_exec($curl);
					//curl_close($curl);
					$session_data = array(
							'customer_id' => $response->application_id,
							'customer_email' => self::$ci->session->userdata( 'email' )
						);
					self::$ci->session->set_userdata($session_data);
					redirect(base_url().'potwierdzenie');

				} else if ($response->status == 'CUSTOMER_INVALID') {

					#$url = 'http://www.google-analytics.com/collect?v=1&tid=UA-43038387-3&cid=0' . '&t=event&ec=Error&ea='. serialize($response->errors) . '&el=0' . '&ni=1';
					#$curl = curl_init($url);
					#curl_exec($curl);
					#curl_close($curl);
					error_log(serialize($response->errors), 3, getcwd() . '/my-errors.log');
					redirect(base_url().'blad#' . serialize($response->errors));

				} else if ($response->status == 'REJECTED')  {

					$gender = self::$ci->input->post( 'personal_id' );
					if($gender[9] % 2 == 0) {
						$gender = 'FEMALE';
					} else {
						$gender = 'MALE';
					}

					$occupation = self::$ci->input->post( 'occupation' );
					if($occupation == "pensioner2"){
						$occupation = "pensioner1";
					}


					$employer = self::$ci->input->post( 'employer_name' );
					if(empty($employer)){
						$employer = "";
					}

					$employer_work_city =  self::$ci->input->post( 'employer_work_city' );
					if(empty($employer_work_city)){
						$employer_work_city = "";
					}
					else{
						$employer_work_city = self::$ci->input->post('employer_work_street_number') . ', ' . self::$ci->input->post('employer_work_postal_code') . ', '. self::$ci->input->post( 'employer_work_city' );
					}

					$education = self::$ci->input->post( 'education' );

					$car = self::$ci->input->post( 'car' );
					if($car != 'yes'){
						$car = "no";
					}

					$current_job_position = self::$ci->input->post('current_job_position');

					$timestamp = new DateTime(date('Y-m-d'));
 
					$dimplode = array(
					"first_name", self::$ci->session->userdata( 'first_name' ),  // from form
					"last_name" , self::$ci->session->userdata( 'last_name' ),  // from form
					"university_name" , self::$ci->input->post( 'university_name' ), // from form
					"university_city" ,self::$ci->input->post( 'university_city' ),  // from form
					"id_card_number" , self::$ci->input->post( 'id_card_number' ),  //from form
					"personal_id" ,  self::$ci->input->post( 'personal_id' ), //from form
					"bank_account_number", preg_replace('/[^0-9]+/', '', self::$ci->input->post('bank_account')),
					"phone" , self::$ci->session->userdata( 'phone' ), //from form
					//"employer_phone" , "", // hardcoded
					"email", self::$ci->session->userdata( 'email' ), // from form
					"street" , self::$ci->input->post( 'address' ), //from form
					"house_number" , self::$ci->input->post( 'house_number' ), //from form
					"city" ,self::$ci->input->post( 'city' ), //from form
					"postal_index" , str_replace('-', '', self::$ci->input->post( 'postal_index' )), //from form
					"lives_at_registered_address" , "1", //hardcoded
					"secondary_city" , null, //hardcoded
					"secondary_street" , null, //hardcoded
					"secondary_house_number" , "", //hardcoded
					"secondary_postal_index" , null, //hardcoded
					"occupation" , $occupation, //from form
					"current_job_position" , $current_job_position, //hardcoded
					"current_employment_months" , $current_employment_months, //from form
					"current_employment_years" , $current_employment_years, //from form
					"remuneration_deadline" ,  $remu_date1, //from form
					"salary_to_personal_account" , self::$ci->input->post( 'customer_salary_to_personal_account' ), //from form
					"business_registration_date" , $bus_data , //hardcoded
					"employer" , $employer, // from form
					"employer_work_city" , $employer_work_city, // from form
					"neto_income" , self::$ci->input->post( 'neto_income' ), //from form
					//"monthly_expenses" , "", //hardcoded
					"gender" , $gender,  // form form
					"marital_status" , self::$ci->input->post( 'marital_status' ), //from form
					"education" , $education, //from form
					//"remuneration_frequency" , null, //hardcoded
					"housing_type" , self::$ci->input->post( 'housing_type' ), //form form
					"housing_type_lived_months" , self::$ci->input->post( 'housing_type_lived_months' ) , //from form
					"housing_type_lived_years" , self::$ci->input->post( 'housing_type_lived_years' ), //form form
					"car" , $car, // form form
					//"industry" , null, //hardcoded
					"affiliate" , "Kobietazkasa.pl",  //hardcoded
					"ip_address" , self::$ci->input->ip_address(), // from form
					"loan_sum" , self::$ci->session->userdata( 'value' ), // from form
					"loan_period" , self::$ci->session->userdata( 'period' ), //from form
					"agree_electronic_services" , self::$ci->session->userdata( 'agree_electronic_services' ), //hardcoded
					"agree_personal_data_protection" , self::$ci->session->userdata( 'agree_personal_data_protection' ), //hardcoded
					"agree_data_sharing" , "1" // hardcoded
					//"tax_id_number" , null //hardcoded
					);

					$d = array();
					foreach (array_chunk($dimplode, 2) as $pair) {
						list($key, $value) = $pair;
						$d[$key] = $value;
					}

					$data2 = array(
					'lead' => $d,
					'api_key' =>  'AjL56J4Q8Y',
					'hash' => sha1(implode('', $dimplode).'AjL56J4Q8Y' . $timestamp->getTimestamp()),
					'timestamp' => $timestamp->getTimestamp()
					);

					$content2 = json_encode($data2);

					//$url2 = 'http://tc-test.homenet.org/api/lead';
					//$url2 = 'http://54.72.75.24/api/lead';
					$url2 = 'https://credy.eu/api/lead';

					$curl2 = curl_init($url2);
					curl_setopt($curl2, CURLOPT_HEADER, false);
					curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl2, CURLOPT_HTTPHEADER,
					array('Content-type: application/json'));
					curl_setopt($curl2, CURLOPT_POST, true);
					curl_setopt($curl2, CURLOPT_POSTFIELDS, $content2);
					curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);

					$result2     = curl_exec($curl2);
					$response2   = json_decode($result2);
					curl_close($curl2);

					if($response2->lead_id == null ){

						//$message = "\r\n" . http_build_query($response2) .  " : " . implode(' ', $dimplode) . "\r\n";
						//error_log($message, 3, getcwd() . '/my-errors.log');
						redirect(base_url(). 'informacja');

					}

					redirect(base_url().'posiadasz-juz-konto?lead=' . $response2->lead_id);

				} else {

					//$url = 'http://www.google-analytics.com/collect?v=1&tid=UA-43038387-3&cid=0' . '&t=event&ec=Error&ea='. serialize($response->errors) . '&el=0' . '&ni=1';
					//$curl = curl_init($url);
					//curl_exec($curl);
					//curl_close($curl);
					error_log('unexpected: '.serialize($response->errors), 3, getcwd() . '/my-errors.log');
					redirect(base_url().'blad');

				}



			}

		}

	}
}
