		<div class="twoja_pozyczka">
		        	<h3><strong>Twoja</strong> pożyczka</h3>
		            <form method="post" action="">
		            	<fieldset class="kolo1">
		                	<h5>Pożyczam</h5>
		                    <div>
		                    	<strong><?php if(isset($_SESSION['value'])): ?><?php echo $_SESSION['value'] ?><?php else: ?>750<?php endif ?></strong> zł
		                        <input type="hidden" name="value" id="kwota" value="<?php if(isset($_SESSION['value'])): ?><?php echo $_SESSION['value'] ?><?php else: ?>750<?php endif ?>">
		                    	<a class="plus">plus</a>
		                    	<a class="minus">minus</a>
		                    </div>
		                    <p>(100-1 000 zł)</p>
		                </fieldset>
		            	<fieldset class="kolo2">
		                	<h5>Na czas</h5>
		                    <div>
		                    	<strong><?php if(isset($_SESSION['period'])): ?><?php echo $_SESSION['period'] ?><?php else: ?>27<?php endif ?></strong> dni
		                        <input type="hidden" name="period" id="czas" value="<?php if(isset($_SESSION['period'])): ?><?php echo $_SESSION['period'] ?><?php else: ?>27<?php endif ?>">
		                    	<a class="plus">plus</a>
		                    	<a class="minus">minus</a>
		                    </div>
		                    <p>(7-31 dni)</p>
		                </fieldset>
		            	<fieldset class="kolo3">
		                	<p class="t1">Spłata do <span class="to"></span> <br>Pożyczka <span class="l_value"></span> zł</p>
							<input type="hidden" name="form" value="kalkulator"/>
		                    <button type="submit"><strong>Weź</strong> pożyczkę</button>
		                    <p class="t2">Prowizja <span class="fee"></span> zł <br>Razem <span class="total"></span> zł</p>
		                    <p class="t3">RRSO <span class="rrso"></span>%</p>
		                </fieldset>
		            </form>
		        </div>
				
				