<footer>
    	<a href="mailto:kontakt@kobietazkasa.pl">kontakt@kobietazkasa.pl</a>
        <p class="kontakt">Kontakt z Konsultantką <br><strong>+48 22 388 32 10</strong></p>
		<ion:navigation tag="ul">
			<li<ion:is_active> class="active"</ion:is_active>>
				<a href="<ion:url />"><ion:title /></a>
			</li>
		</ion:navigation>
        <p>Wszelkie Prawa zastrzeżone <?php echo date('Y') ?> | Kobietazkasa.pl</p>
        
        <div>
        	<p>Friendly Finance Poland spółka z ograniczoną odpowiedzialnością z siedzibą w Warszawie 00-019 przy ul. Złotej 7/18, telefon +48 22 112 0 112. NIP: 1070025046, REGON: 146512445. Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego KRS: 0000448749. Kapitał zakładowy w wysokości 2.000 000,00 PLN </p>
        	<p>Jednorazowa opłata rejestracyjna wynosi 0,01 PLN. Reprezentatywny przykład: całkowita kwota pożyczki 800 PLN; czas obowiązywania umowy 31 dni; oprocentowanie 0%; opłata przygotowawcza 0%; prowizja 214,4 PLN; całkowita kwota do zapłaty 1014,4 PLN; RRSO 1537.6% (stan na dzień 13.07.2015 r.).</p>
        	<p>Dodatkowe koszty związane z pożyczką mogą być naliczone wyłącznie w momencie braku terminowej spłaty udzielonej pożyczki. W przypadku opóźnienia w spłacie Pożyczki, Pożyczkodawca zastrzega sobie możliwość naliczenia odsetek za zwłokę w wysokości czterokrotnej wysokości stopy kredytu lombardowego Narodowego Banku Polskiego oraz podjęcia działań mających na celu zwrot Pożyczki przez Pożyczkobiorcę. W szczególności Pożyczkodawca wezwie Pożyczkobiorcę do zapłaty telefonicznie lub wysyłając SMS, e-mail lub pisemne wezwanie do zapłaty. Pożyczkobiorca zostanie obciążony kosztami tych monitów zgodnie z warunkami obowiązującej Tabeli Opłat i Prowizji. Kosztami dochodzenia zwrotu Pożyczki na drodze postępowania sądowego zostanie obciążony Pożyczkobiorca.W przypadku łącznego spełnienia przesłanek określonych w artykule 14 lub 16 bądź 17 ustawy z dnia 9 kwietnia 2010 r. o udostępnianiu informacji gospodarczych i wymianie danych gospodarczych (Dz.U. Nr 81, poz. 530) Pożyczkodawca ma prawo przekazać informacje o zobowiązaniach Pożyczkobiorcy do Biur Informacji Gospodarczej, co może mieć wpływ na zdolność kredytową Pożyczkobiorcy. Przekazanie danych Pożyczkobiorcy do Biur Informacji Gospodarczej jest dokonywane na podstawie pisemnej umowy o udostępnianie informacji gospodarczych zawartej przez Pożyczkodawcę z Biurem Informacji Gospodarczej oraz w oparciu o przepisy rozdziału 3 ustawy o udostępnianiu informacji gospodarczych i wymianie danych gospodarczych.Przedłużenie spłaty pożyczki nie odbywa się automatycznie i jest zależne od woli klienta. Przedłużenie terminu spłaty odbywa się po dokonaniu opłaty za przedłużenie, której wysokość zależy od kwoty pożyczki i terminu przedłużenia.</p>
       	</div>

       	<?php /*
        <!--<p>Friendly Finance Poland spółka z ograniczoną odpowiedzialnością z siedzibą w Warszawie 00-019 przy ul. Złotej 7/18, telefon +48 22 388 32 10. NIP: 1070025046, REGON: 146512445. Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego KRS: 0000448749. Kapitał zakładowy w wysokości 2.000 000,00 PLN</p>
        --><div>
            <p>
                Jednorazowa opłata rejestracyjna wynosi 0,01 PLN. Reprezentatywny przykład: całkowita kwota pożyczki 800 PLN; czas obowiązywania umowy 31 dni; oprocentowanie 0%; opłata przygotowawcza 0%; prowizja 214,4 PLN; całkowita kwota do zapłaty 1014,4 PLN; RRSO 1537.6% (stan na dzień 13.07.2015 r.).
            </p>
            <p>Dodatkowe koszty związane z pożyczką mogą być naliczone wyłącznie w momencie braku terminowej spłaty udzielonej pożyczki. W przypadku opóźnienia w spłacie Pożyczki, Pożyczkodawca zastrzega sobie możliwość naliczenia odsetek za zwłokę w wysokości czterokrotnej wysokości stopy kredytu lombardowego Narodowego Banku Polskiego oraz podjęcia działań mających na celu zwrot Pożyczki przez Pożyczkobiorcę. W szczególności Pożyczkodawca wezwie Pożyczkobiorcę do zapłaty telefonicznie lub wysyłając SMS, e-mail lub pisemne wezwanie do zapłaty. Pożyczkobiorca zostanie obciążony kosztami tych monitów zgodnie z warunkami obowiązującej Tabeli Opłat i Prowizji. Kosztami dochodzenia zwrotu Pożyczki na drodze postępowania sądowego zostanie obciążony Pożyczkobiorca.</p>
            <p>W przypadku łącznego spełnienia przesłanek określonych w artykule 14 lub 16 bądź 17 ustawy z dnia 9 kwietnia 2010 r. o udostępnianiu informacji gospodarczych i wymianie danych gospodarczych (Dz.U. Nr 81, poz. 530) Pożyczkodawca ma prawo przekazać informacje o zobowiązaniach Pożyczkobiorcy do Biur Informacji Gospodarczej, co może mieć wpływ na zdolność kredytową Pożyczkobiorcy. Przekazanie danych Pożyczkobiorcy do Biur Informacji Gospodarczej jest dokonywane na podstawie pisemnej umowy o udostępnianie informacji gospodarczych zawartej przez Pożyczkodawcę z Biurem Informacji Gospodarczej oraz w oparciu o przepisy rozdziału 3 ustawy o udostępnianiu informacji gospodarczych i wymianie danych gospodarczych.</p>
            <p>Przedłużenie spłaty pożyczki nie odbywa się automatycznie i jest zależne od woli klienta. Przedłużenie terminu spłaty odbywa się po dokonaniu opłaty za przedłużenie, której wysokość zależy od kwoty pożyczki i terminu przedłużenia. </p>
            <p>Weryfikację danych użytkowników obsługuje CashBill Spółka Akcyjna z siedzibą w Dąbrowie Górniczej przy
            ul. Rejtana 20 NIP: 644-287-37-80 REGON: 2411035843.<br>
            Operator świadczy usługę weryfikacji zgodności danych podanych przez użytkownika w procesie aplikowania o pożyczkę
            (imię, nazwisko, adres zamieszkania, numer konta). <a href="http://www.cashbill.pl">www.cashbill.pl</a>
            <br><br>
            Użytkownik może zrezygnować z automatycznego potwierdzenia danych i dokonać płatności 1gr na wskazane przez serwis konto.</p>
        </div>
        */ ?>
    </footer>
	<?php
	if(strpos($_SERVER['REQUEST_URI'], 'aff=') !== false) {
		$aff = explode('aff=', $_SERVER['REQUEST_URI']);
		$aff = explode('&', $aff[1]);

		if($aff[0] == 'doaff'):
	?>
		<script type="text/javascript">
		    if (window.location.href.indexOf("d_aid")>-1)<!--
		        document.write(unescape("%3Cscript id='doaffiliate' src='" + (("https:" == document.location.protocol) ? "https://" : "http://")
		        + "tracker2.doaffiliate.net/js/doaffclick.js' type='text/javascript'%3E%3C/script%3E"));//-->
		</script>
		<script type="text/javascript">
		    if (window.location.href.indexOf("d_aid")>-1){
		        DoAffTracker.setMerchantId(168);
		        DoAffTracker.setCampaignId(181);
		        DoAffTracker.setPublicKey('<?php echo md5("168_181_".rand(0,10000)."_".time());?>');
		        DoAffTracker.setVisitorId('<?php echo md5(md5("168_181_".rand(0,10000)."_".time())."W~h3nyG*R2|;|DY-_VuZ}iXp,b?/T>re5BvQI<0oLk)1EN#j4`$:sCSfFt9z[(H6");?>');
		        try {DoAffTracker.trackClick();} catch (err) { }
		    }
		</script>

		<?php
		elseif($aff[0] == 'ebroker'):
		?>

		<script type="text/javascript" src="//tracking.leadaff.pl/la_sign.js"></script>

	<?php endif ?>
	<?php
	}
	?>
	
    <!-- Kod tagu remarketingowego Google -->

    <!--

    Tagi remarketingowe nie mogš być wišzane z informacjami umożliwiajšcymi identyfikację osób ani umieszczane na stronach o tematyce należšcej do kategorii kontrowersyjnych. Więcej informacji oraz instrukcje konfiguracji tagu znajdziesz tutaj: http://google.com/ads/remarketingsetup

    -->

    <script type="text/javascript">

    /* <![CDATA[ */

    var google_conversion_id = 962812905;

    var google_custom_params = window.google_tag_params;

    var google_remarketing_only = true;

    /* ]]> */

    </script>

    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

    </script>

    <noscript>

    <div style="display:inline;">

    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/962812905/?value=0&amp;guid=ON&amp;script=0"/>

    </div>

    </noscript>

    <script type="text/javascript" >
var reachlocalTRKDOM="rtsys.rtrk.pl";
(function() {
var rlocal_load = document.createElement("script");
rlocal_load.type = "text/javascript";
rlocal_load.src = document.location.protocol+"//"+reachlocalTRKDOM+"/rct_lct/js/rlrct1.js";
(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild (rlocal_load);
})(); </script>

</body>
</html>
