<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7"> <![endif]-->
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<head>
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><ion:meta_title /> <ion:site_title /></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <meta name="description" content="<ion:meta_description />">

    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="stylesheet" href="<ion:theme_url />assets/css/core.css" />
    <link rel="stylesheet" href="<ion:theme_url />assets/css/style.css" />
    <link rel="stylesheet" href="<ion:theme_url />assets/css/jquery.cookiebar.css" />

    <script src="<ion:theme_url />assets/js/plugins/jquery-1.9.1.min.js"></script>
    <script src="<ion:theme_url />assets/js/plugins/jquery.selectBox.js"></script>
    <script src="<ion:theme_url />assets/js/plugins/jquery.bxslider.js"></script>
    <script src="<ion:theme_url />assets/js/jquery.cookiebar.js"></script>

    <script src="<ion:theme_url />assets/js/init.js"></script>

    <!--[if IE]>
    <script src="code/js/plugins/placeholder.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="code/js/libs/html5shiv.js"></script>
    <![endif]-->

    <ion:google_analytics />
</head>
<body class="home">
    
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P28W3T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P28W3T');</script>
<!-- End Google Tag Manager -->

	<header class="centralize clearfix">
    	<h1><a href="/"><img src="<ion:theme_url />assets/images/logo.png" width="244" height="140"></a></h1>
        <nav>
			<ion:navigation tag="ul">
				<li<ion:is_active> class="active"</ion:is_active>>
					<a href="<ion:url />"><ion:title /></a>
				</li>
			</ion:navigation>
        </nav>
        <p class="kontakt">Kontakt z Konsultantką <strong>22 388 32 10</strong></p>
    <!--    <p class="partner">Partner serwisu Pożyczkomat.pl</p> -->
    </header>
