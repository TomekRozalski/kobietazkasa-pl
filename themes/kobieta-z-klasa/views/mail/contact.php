<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><ion:data:subject /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="<ion:current_lang />" />
</head>
<body>
 
<table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="bg_fade">
            <table border="0" width="880">
                <tr>
                    <td>
                       
                        <p>
                            Imię i nazwisko: <b><ion:data:name /></b>,<br/>
                            E-mail: <b><ion:data:email /></b>
                        </p>
 
                        <p>
                            Wiadomość:<br/>
							<ion:data:message />
                        </p>
 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
 
</body>
</html>