<ion:partial view="header" />

<section class="page_wrapper centralize poradniki">
	
	<div class="hero">
	        	<h2>Stylebook</h2>
	    		
				<ion:page:articles type="intro">

							<ion:article>
							                    <ion:content/>
					
							</ion:article>
				
					</ion:page:articles>
				
	        </div>
	
	
	<div class="box">

	<ion:page:articles range="1,5" type="NULL">

				<ion:article>
				
					<div class="porada clearfix <ion:article:index expression="%2==1">left</ion:article:index><ion:article:index expression="%2==0">right</ion:article:index>">
				            	<div>
				                	<h3><ion:title class="pagetitle" /></h3>
				                    <ion:content paragraph="1" />
				                	<a href="<ion:url />" class="btn">zobacz poradę</a>
				                </div>
								<ion:medias type="picture" limit="1">

									<ion:media:type is="picture"> <img src="<ion:media:src />" /> </ion:media:type>

								</ion:medias> <!-- ><br> <span class="h">99</span> <span class="l">99</span> -->
							
					</div>
					
				</ion:article>

	</ion:page:articles>
	
	<div class="zobacz_takze" style="display:none">
		
	            	<h3>Zobacz także</h3>
	                <br clear="all">
					
					<ion:page:articles range="4,99" type="NULL">

								<ion:article>
								
								
				                <a href="<ion:url />">
				                	<span class="title"><ion:title class="pagetitle" /></span>
									<ion:medias type="picture" limit="1">

										<ion:media:type is="picture"> <img src="<ion:media:src size='175,155' method='adaptive' />" /> </ion:media:type>

				            		</ion:medias><!--<span class="h">99</span> <span class="l">99</span>-->
				                </a>
					
								</ion:article>

					</ion:page:articles>
					
	            </div>

			</div>
			
			<ion:partial view="calculator" />	

</section>

<ion:partial view="footer" />