<ion:partial view="header" />

<section class="page_wrapper centralize poradniki">
	
	<div class="hero">
	        	<h2>Stylebook</h2>
	    		
				<ion:page:articles type="intro">

							<ion:article>
							                    <ion:content/>
					
							</ion:article>
				
					</ion:page:articles>
				
	        </div>
	
	
	<div class="box">

	<div class="porada_full clearfix">
	            	<div>
						<ion:article>
	                	<h3><ion:title /></h3>
						<ion:medias type="picture" limit="1">
						<div class="art-img">
							<ion:media:type is="picture"> <img src="<ion:media:src size='285,164' method='adaptive' />" /> </ion:media:type>
						</div>
	            		</ion:medias><br>
	                   <!-- <span class="h">99</span>
	                    <span class="l">99</span> -->
	                    <h5><ion:subtitle /></h5>
	                    <ion:content />
	                    <a href="/stylebook" class="btn">wróć do porad</a>
						</ion:article>
	                </div>
	</div>
	
	<div class="zobacz_takze">
		
	            	<h3>Zobacz także</h3>
	                <br clear="all">
					
					<ion:page:articles range="1,9" type="NULL">

								<ion:article>
								
								
				                <a href="<ion:url />">
				                	<span class="title"><ion:title class="pagetitle" /></span>
									<ion:medias type="picture" limit="1">

										<ion:media:type is="picture"> <img src="<ion:media:src size='175,155' method='adaptive' />" /> </ion:media:type>

				            		</ion:medias><!--<span class="h">99</span> <span class="l">99</span>-->
				                </a>
					
								</ion:article>

					</ion:page:articles>
					
	            </div>

			</div>
			
			<ion:partial view="calculator" />	

</section>

<ion:partial view="footer" />