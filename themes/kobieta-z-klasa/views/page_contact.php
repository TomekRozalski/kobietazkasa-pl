<ion:partial view="header" />

<section class="page_wrapper centralize form kontakt_form">
        <ion:page:articles type="">

      <ion:article>
                <ion:title tag="h2" />
                <ion:content />
      </ion:article>

        </ion:page:articles>

        <div class="box">
		<a name="sent"></a>
        <h2>Formularz</h2>

	  <ion:form:contact:validation:success tag="small" class="alert" />
		
          <form method="post" action="">
              <fieldset>
				  
                  <div class="form_row">
                      <label for="name">Imię i nazwisko</label>
            <input name="name" type="text" class="req" id="name" value="<ion:form:contact:field:name />"<ion:form:contact:error:name is="true"> class="error" </ion:form:contact:error:name>/>
            <ion:form:contact:error:name tag="small" class="error" />
                    </div>
                  <div class="form_row">
                      <label for="email">Adres e-mail</label>
            <input name="email" class="req email" type="text" id="email" value="<ion:form:contact:field:email />"<ion:form:contact:error:email is="true"> class="error" </ion:form:contact:error:email> />
            <ion:form:contact:error:email tag="small" class="error" />
                    </div>
                  <div class="form_row">
                      <label for="subject">Temat</label>
            <input name="subject" class="req" type="text" id="subject" value="<ion:form:contact:field:subject />"<ion:form:contact:error:subject is="true"> class="error" </ion:form:contact:error:subject> />
            <ion:form:contact:error:subject tag="small" class="error" />
                    </div>
                  <div class="form_row">
                      <label for="message">Wiadomość</label>
            <textarea name="message" class="req" id="message" rows="7"<ion:form:contact:error:message is="true"> class="error" </ion:form:contact:error:message>></textarea>
            <ion:form:contact:error:message tag="small" class="error" />
                    </div>
					<input type="hidden" name="form" value="contact" />
					<input type="hidden" name="tag" value="sent" />
                </fieldset>
                <button type="submit">Wyślij</button>
            </form>
        </div>

<ion:partial view="calculator" />	
		
    </section>

<ion:partial view="footer" />
