<ion:partial view="header" />

<section class="page_wrapper centralize form full_width kontakt_form">
	
	<div class="box">
	
        <ion:page:articles type="">

			<ion:article>
                <ion:title tag="h2" />
                <ion:content />
			</ion:article>

        </ion:page:articles>
		
	</div>

    </section>

<ion:partial view="footer" />
