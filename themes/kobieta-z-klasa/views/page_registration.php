<ion:partial view="header" />

<section class="page_wrapper centralize form">
         <ion:page:articles type="">

       <ion:article>
                 <ion:title tag="h2" />
                 <ion:content />
       </ion:article>

         </ion:page:articles>

	           <div class="box">
	           	<form method="post" action="">
	               	<fieldset>
	                   	<h3>Załóż konto</h3>
						
						<ion:form:rejestracja:validation:error is="true" tag="p" class="alert" >
						        Prosimy poprawnie wypełnić oznaczone pola.
						</ion:form:rejestracja:validation:error>
						
							                       <p>Twoje dane są u nas bezpieczne - szyfrujemy je certyfikatem SSL.</p>
						
	                   	<div class="form_row">
	                       	<label for="first_name">Imię</label>
	                           <input type="text" name="first_name" id="first_name" value="<ion:form:rejestracja:field:first_name />" class="req<ion:form:rejestracja:error:first_name is="true"> error</ion:form:rejestracja:error:first_name>">
	                       </div>
						   
	                   	<div class="form_row">
	                       	<label for="last_name">Nazwisko</label>
	                           <input type="text" name="last_name" id="last_name"  value="<ion:form:rejestracja:field:last_name />" class="req<ion:form:rejestracja:error:last_name is="true">  error</ion:form:rejestracja:error:last_name>">
	                       </div>
						   
	                   	<div class="form_row">
	                       	<label for="birth_date_3i">Data urodzenia</label>
	                           <select name="birth_date_3i" id="birth_date_3i" class="req day<ion:form:rejestracja:error:birth_date_3i is="true"> error</ion:form:rejestracja:error:birth_date_3i>">
	                           	<option value="">--</option>
								<?php for($i = 1; $i <= 31; $i++): ?>
	                               <option value="<?php echo $i; ?>"<?php if($i == '<ion:form:rejestracja:field:birth_date_3i />'): ?> selected="selected"<?php endif ?>><?php echo $i; ?></option>
							   <?php endfor; ?>
	                           </select>
							   
	                           <select name="birth_date_2i" id="birth_date_2i" class="req month<ion:form:rejestracja:error:birth_date_2i is="true">  error</ion:form:rejestracja:error:birth_date_2i>">
	                           	<option value="">----</option>
									<?php 
									$miesiac = array( '', 'styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień' );
									for($i = 1; $i <= 12; $i++): ?>
		                               <option value="<?php echo $i; ?>"<?php if($i == '<ion:form:rejestracja:field:birth_date_2i />'): ?> selected="selected"<?php endif ?>><?php echo $miesiac[$i]; ?></option>
								   <?php endfor; ?>
	                           </select>
							   
	                           <select name="birth_date_1i" id="birth_date_1i" class="req year<ion:form:rejestracja:error:birth_date_1i is="true"> error</ion:form:rejestracja:error:birth_date_1i>">
	                           	<option value="">--</option>
									<?php for($i = date('Y')-18; $i >= date('Y')-80; $i--): ?>
		                               <option value="<?php echo $i; ?>"<?php if($i == '<ion:form:rejestracja:field:birth_date_1i />'): ?> selected="selected"<?php endif ?>><?php echo $i; ?></option>
								   <?php endfor; ?>
	                           </select>
	                       </div>
						   
	                   	<div class="form_row">
	                       	<label for="email">Adres e-mail</label>
	                           <input type="text" name="email" id="email" value="<ion:form:rejestracja:field:email />" class="req  email<ion:form:rejestracja:error:email is="true"> error</ion:form:rejestracja:error:email>">
	                       </div>
						   
	                   	<div class="form_row">
	                       	<label for="phone">Telefon komórkowy</label>
	                           <span>+48</span>
	                           <input type="text" name="phone" id="phone" value="<ion:form:rejestracja:field:phone />" class="req number<ion:form:rejestracja:error:phone is="true"> error</ion:form:rejestracja:error:phone>" style="width:250px;" maxlength="9">
	                       </div>
						   
	                   	<div class="form_row">
	                       	<label for="password">Hasło</label>
	                           <input type="password" name="password" id="password" value="<ion:form:rejestracja:field:password />" class="req pwd<ion:form:rejestracja:error:password is="true"> error</ion:form:rejestracja:error:password>">
	                       </div>
						   
	                   	<div class="form_row">
	                       	<label for="password2">Powtórz hasło</label>
	                           <input type="password" name="password2" id="password2"  value="<ion:form:rejestracja:field:password2 />" class="req re_pwd<ion:form:rejestracja:error:password2 is="true">  error</ion:form:rejestracja:error:password2>">
	                       </div>
						   
	                       <label class="chck" for="agree_rules">
	                       	<input type="checkbox" name="agree_rules" id="agree_rules" class="tick req" value="1">
	                           Akceptuję <a href="/regulamin" target="_blank">Regulamin świadczenia usług drogą elektroniczną</a>.
	                       </label>
						   
	                       <label class="chck small-text" for="agree_electronic_services">
	                       	<input type="checkbox" name="agree_electronic_services" id="agree_electronic_services" class="tick req" value="1">
	                           Wyrażam zgodę na przekazanie przez Friendly Finance Poland sp. z o.o. moich danych osobowych (i) innym podmiotom działającym w Polsce <span>na rynku pożyczek krótkoterminowych zawieranych na odległość, (ii) pośrednikom finansowym, którzy mogą przekazywać moje dane osobowe innym podmiotom działającym w Polsce na rynku pożyczek krótkoterminowych, a także wyrażam zgodę na przetwarzanie moich danych osobowych przez te podmioty celem podjęcia czynności niezbędnych przed zawarciem umów, w tym weryfikacji zdolności kredytowej, w przypadku gdy w wyniku badania zdolności kredytowej Friendly Finance Poland sp. z o.o. odmówi mi udzielenia pożyczki.
	                       </label>
						   
	                       <label class="chck" for="agree_personal_data_protection">
	                       	<input type="checkbox" name="agree_personal_data_protection" id="agree_personal_data_protection" class="tick req" value="1">
	                           Akceptuję <a href="/warunki-ochrony-danych-osobowych" target="_blank">warunki ochrony danych osobowych</a>.
	                       </label>
						   
	                   </fieldset>
					   <input type="hidden" name="form" value="rejestracja">
					   <input type="hidden" name="next" value="dane">
	                   <button type="submit">Dalej</button>
	               </form>
	           </div>
	           <div class="twoja_pozyczka">
	           	<h3><strong>Twoja</strong> pożyczka</h3>
	           	<form class="locked">
	               	<fieldset class="kolo1">
	                   	<h5>Pożyczam</h5>
	                       <div>
	                    	<strong><?php if(isset($_SESSION['value'])): ?><?php echo $_SESSION['value'] ?><?php else: ?>600<?php endif ?></strong> zł
	                        <input type="hidden" name="value" id="kwota" value="<?php if(isset($_SESSION['value'])): ?><?php echo $_SESSION['value'] ?><?php else: ?>600<?php endif ?>">
	                       </div>
	                   </fieldset>
	               	<fieldset class="kolo2">
	                   	<h5>Na czas</h5>
	                       <div>
	                    	<strong><?php if(isset($_SESSION['period'])): ?><?php echo $_SESSION['period'] ?><?php else: ?>7<?php endif ?></strong> dni
	                        <input type="hidden" name="period" id="czas" value="<?php if(isset($_SESSION['period'])): ?><?php echo $_SESSION['period'] ?><?php else: ?>7<?php endif ?>">
	                       </div>
	                   </fieldset>
	               </form>
	           </div>
	           <img src="<ion:theme_url />assets/images/pic.png" class="pic">
       
    </section>

<ion:partial view="footer" />
