<ion:partial view="header" />

<section class="page_wrapper centralize opinie">
	
	<div class="hero">
	        	<h2>Kobiety o Nas</h2>
				
				<div class="box">
	    		
				<ion:page:articles type="intro">

							<ion:article>
							                    <ion:content/>
					
							</ion:article>
				
					</ion:page:articles>
				
	        </div>
	
	<ion:page:articles type="NULL">

				<ion:article>
			
			<div class="opinia clearfix">
			        	<h5><ion:subtitle /></h5>
			            <h4><ion:title /></h4>
			            <ion:content/>
			            <ion:media:type is="picture"> <img src="<ion:media:src />" /> </ion:media:type>
			        </div>
	
				</ion:article>

	</ion:page:articles>
	
</div>
	
	<ion:partial view="calculator" />	

</section>

<ion:partial view="footer" />