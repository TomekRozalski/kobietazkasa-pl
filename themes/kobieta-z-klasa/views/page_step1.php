<ion:partial view="header" />

<section class="page_wrapper centralize form">
    	<h2>Dane do pożyczki</h2>

        <div class="box">

	           	<form method="post" action="">
            	<fieldset>
                	<h3>Podaj dane potrzebne <br>do złożenia wniosku</h3>

					<ion:form:dane:validation:error is="true" tag="p" class="alert" >
					        Prosimy poprawnie wypełnić oznaczone pola.
					</ion:form:dane:validation:error>

                	<div class="form_row">
                    	<label for="personal_id">PESEL</label>
                        <input type="text" name="personal_id" id="personal_id" value="<ion:form:dane:field:personal_id />" class="req pesel<ion:form:dane:error:personal_id is="true"> error</ion:form:dane:error:personal_id>">
                    </div>

                	<div class="form_row">
                    	<label for="id_card_number">Numer dowodu osobistego</label>
                        <input type="text" name="id_card_number" id="id_card_number" value="<ion:form:dane:field:id_card_number />" class="req idcard<ion:form:dane:error:id_card_number is="true"> error</ion:form:dane:error:id_card_number>">
                    </div>

                    <!-- <div class="form_row">
                        <label for="mothers_maiden_name">Nazwisko panieńskie matki</label>
                        <input type="text" name="mothers_maiden_name" id="mothers_maiden_name" value="<ion:form:dane:field:mothers_maiden_name />" class="req<ion:form:dane:error:mothers_maiden_name is="true"> error</ion:form:dane:error:mothers_maiden_name>">
                    </div> -->

                	<div class="form_row">
                    	<label for="education">Wykształcenie</label>
                        <select name="education" id="education" class="req<ion:form:dane:error:education is="true"> error</ion:form:dane:error:education>">
							<option value="">----</option>
							<?php
							$education = array( '', 'no_education', 'basic_school', 'high_school', 'industrial_school', 'bachelor', 'master', 'phd' );
                            $education2 = array( '', 'Brak', 'Gimnazjalne', 'Średnie', 'Zasadnicze zawodowe', 'Licencjackie', 'Wyższe', 'Podyplomowe' );
							for($i = 1; $i <= 7; $i++): ?>
							<option value="<?php echo $education[$i] ?>"<?php if($education[$i] == '<ion:form:dane:field:education />'): ?> selected="selected"<?php endif ?>><?php echo $education2[$i]?></option>
							<?php endfor ?>
                        </select>
                    </div>

                	<div class="form_row">
                    	<label for="marital_status">Stan cywilny</label>
                        <select name="marital_status" id="marital_status" class="req<ion:form:dane:error:marital_status is="true"> error</ion:form:dane:error:marital_status>">
							<option value="">----</option>
							<?php
							$marital_status = array( '', 'single', 'divorced', 'married', 'with_partner', 'widow' );
							$marital_status2 = array( '', 'Wolny', 'Rozwiedziona/Rozwiedziony', 'Mężatka/Żonaty', 'Związek partnerski', 'Wdowa/Wdowiec' );
							for($i = 1; $i <= 5; $i++): ?>
							<option value="<?php echo $marital_status[$i] ?>"<?php if($marital_status[$i] == '<ion:form:dane:field:marital_status />'): ?> selected="selected"<?php endif ?>><?php echo $marital_status2[$i]?></option>
							<?php endfor ?>
                        </select>
                    </div>




                	<!-- <div class="form_row inline"> -->
                    <div class="form_row">
                    <label for="car">Czy posiadasz samochód</label>
                    <!-- <label for="car" class="medium">Czy posiadasz samochód</label> -->
                        <select name="car" id="car" class="req medium<ion:form:dane:error:car is="true"> error</ion:form:dane:error:car>">
							<option value="">----</option>
							<?php
							$car = array( '', 'no', 'leasing', 'yes', 'business' );
							$car2 = array( '', 'Nie', 'W leasingu', 'Tak', 'Służbowy' );
							for($i = 1; $i <= 4; $i++): ?>
							<option value="<?php echo $car[$i] ?>"<?php if($car[$i] == '<ion:form:dane:field:car />'): ?> selected="selected"<?php endif ?>><?php echo $car2[$i]?></option>
							<?php endfor ?>
                        </select>
                    </div>

                	<div class="form_row">
                    	<label for="housing_type">Status zamieszkania</label>
                        <select name="housing_type" id="housing_type" class="req<ion:form:dane:error:housing_type is="true"> error</ion:form:dane:error:housing_type>">
							<option value="">----</option>
							<?php
							$housing_type = array( '', 'rented_room', 'rented_apartment_or_house', 'own_house_or_apartment', 'with_parents' );
							$housing_type2 = array( '', 'Wynajęty pokój', 'Wynajęte mieszkanie/Wynajęty dom', 'Własne mieszkanie/Własny dom', 'U rodziny' );
							for($i = 1; $i <= 4; $i++): ?>
							<option value="<?php echo $housing_type[$i] ?>"<?php if($housing_type[$i] == '<ion:form:dane:field:housing_type />'): ?> selected="selected"<?php endif ?>><?php echo $housing_type2[$i]?></option>
							<?php endfor ?>
                        </select>
                    </div>

                	<div class="form_row">
                    	<label for="housing_type_lived_years">Jak długo mieszkasz pod obecnym adresem?</label>
						                        <span>Lata</span>
                        <select name="housing_type_lived_years" id="housing_type_lived_years" class="req short<ion:form:dane:error:housing_type_lived_years is="true"> error</ion:form:dane:error:housing_type_lived_years>">
							<option value="">--</option>
									<?php for($i = 0; $i <= 12; $i++): ?>
                               <option value="<?php echo $i; ?>"<?php if($i == '<ion:form:dane:field:housing_type_lived_years />'): ?> selected="selected"<?php endif ?>><?php echo $i; ?></option>
						   <?php endfor; ?>
                        </select>
						                        <span>Miesiące</span>
                        <select name="housing_type_lived_months" id="housing_type_lived_months" class="req short<ion:form:dane:error:housing_type_lived_months is="true"> error</ion:form:dane:error:housing_type_lived_months>">
							<option value="">--</option>
									<?php for($i = 0; $i <= 11; $i++): ?>
                               <option value="<?php echo $i; ?>"<?php if($i == '<ion:form:dane:field:housing_type_lived_months />'): ?> selected="selected"<?php endif ?>><?php echo $i; ?></option>
						   <?php endfor; ?>
                        </select>
                    </div>

                    <div class="form_row">
                        <label for="bank_account">Numer rachunku bankowego</label>
                        <input type="text" name="bank_account" id="bank_account" value="<ion:form:dane:field:bank_account />" class="req <ion:form:dane:error:bank_account is="true"> error</ion:form:dane:error:bank_account>">
                    </div>

                </fieldset>
                <fieldset>
                	<h3>Twój adres kontaktowy</h3>
                	<div class="form_row">
                    	<label for="address">Ulica</label>
                        <input type="text" name="address" id="address" value="<ion:form:dane:field:address />" class="req<ion:form:dane:error:address is="true"> error</ion:form:dane:error:address>">
                    </div>

                	<div class="form_row">
                    	<label for="house_number">Numer domu</label>
                        <input type="text" name="house_number" id="house_number" value="<ion:form:dane:field:house_number />" class="req code<ion:form:dane:error:house_number is="true"> error</ion:form:dane:error:house_number>">
                    </div>

                	<div class="form_row">
                    	<label for="city">Miasto</label>
                        <input type="text" name="city" id="city" value="<ion:form:dane:field:city />" class="req<ion:form:dane:error:city is="true"> error</ion:form:dane:error:city>">
                    </div>

                	<div class="form_row">
                    	<label for="postal_index">Kod pocztowy</label>
                        <input type="text" name="postal_index" id="postal_index" value="<ion:form:dane:field:postal_index />" class="req kod1<ion:form:dane:error:postal_index is="true"> error</ion:form:dane:error:postal_index>" maxlength="6">
                    </div>

                    <label class="chck" for="cz_lives_at_reqistred_address">
                    	<input type="checkbox" name="cz_lives_at_reqistred_address" id="cz_lives_at_reqistred_address" value="1" class="tick" checked="checked">
                        Pobyt stały jest identyczny z adresem podanym powyżej.
                    </label>
                </fieldset>
				<fieldset class="h_s_1">
							<h3>Miejsce stałego zamieszkania</h3>
							<div class="form_row">
								<label for="customer_secondary_address">Ulica</label>
								<input id="customer_secondary_address" name="secondary_address" size="30" type="text" class="req">
							</div>

							<div class="form_row">
								<label for="customer_secondary_house_number">Numer domu</label>
								<input id="customer_secondary_house_number" name="secondary_house_number" size="30" type="text" class="req code">
							</div>

							<div class="form_row">
								<label for="customer_secondary_city">Miasto</label>
								<input id="customer_secondary_city" name="secondary_city" size="30" type="text" class="req">
							</div>

							<div class="form_row">
								<label for="customer_secondary_postal_index">Kod pocztowy</label>
								<input id="customer_secondary_postal_index" maxlength="6" name="secondary_postal_index" size="6" type="text" class="kod1 req">
							</div>

				</fieldset>
				<fieldset>
					<h3>Informacje dotyczące zatrudnienia</h3>
                	<div class="form_row">
                    	<label for="customer_occupation">Forma zatrudnienia</label>
						<select id="customer_occupation" name="occupation" class="req<ion:form:dane:error:occupation is="true"> error</ion:form:dane:error:occupation>">
							<option value="">----</option>
							<?php
							$customer_occupation = array( '', 'employed_indefinite_period', 'employed_specified_period', 'written_contract_or_order', 'economic_activity', 'unemployed', 'student', 'pensioner1', 'pensioner2', 'other' );
							$customer_occupation2 = array( '', 'Umowa o pracę na czas nieokreślony', 'Umowa o pracę na czas określony', 'Umowa zlecenie/Umowa o dzieło', 'Działalność gospodarcza', 'Bezrobotny', 'Student', 'Emeryt', 'Rencista', 'Inna' );
							for($i = 1; $i <= 9; $i++): ?>
							<option value="<?php echo $customer_occupation[$i] ?>"<?php if($customer_occupation[$i] == '<ion:form:dane:field:occupation />'): ?> selected="selected"<?php endif ?>><?php echo $customer_occupation2[$i]?></option>
							<?php endfor ?>
						</select>
                    </div>
                            <div class="form_row h_r_2">
                                <label for="customer_current_job_position">Stanowisko</label>
                                <input id="customer_current_job_position" name="current_job_position" size="30" type="text" class="req">
                            </div>

							<div class="form_row h_r_1">
								<label for="customer_neto_income">Miesięczny dochód netto</label>
								<input id="customer_neto_income" maxlength="5" name="neto_income" size="6" type="text" class="req kod2"> zł
							</div>


							<div class="form_row h_r_2 h_r_10">
								<label for="customer_employer_name">Nazwa pracodawcy</label>
								<input id="customer_employer_name" name="employer_name" size="30" type="text" class="req">
							</div>

                            <div class="form_row h_r_3">
                                <label for="customer_employer_work_street_number">Ulica i numer lokalu</label>
                                <input id="customer_employer_work_street_number" name="employer_work_street_number" size="30" type="text" class="req">
                            </div>

							<div class="form_row h_r_3">
								<label for="customer_employer_work_city">Miejscowość</label>
								<input id="customer_employer_work_city" name="employer_work_city" size="30" type="text" class="req">
							</div>

                            <div class="form_row h_r_3">
                                <label for="customer_employer_work_postal_code">Kod pocztowy</label>
                                <input id="customer_employer_work_postal_code" maxlength="6" name="employer_work_postal_code" size="6" type="text" class=" kod1 req">
                            </div>

							<div class="form_row h_r_4">
								<label for="customer_remuneration_deadline">Najbliższy termin otrzymania wynagrodzenia</label>
								<select id="customer_remuneration_deadline_3i" name="remuneration_deadline3i" class="short selectBox req">
									<option value="0">----</option>
					<?php for($i = 1; $i <= 31; $i++): ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				   <?php endfor; ?>
					</select>
					&nbsp;					&nbsp;
					<select id="customer_remuneration_deadline_2i" name="remuneration_deadline2i" class="medium selectBox req">
						<option value="0">----</option>
					<?php
					$miesiac = array( '', 'styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień' );
					for($i = 1; $i <= 12; $i++): ?>
                       <option value="<?php echo $i; ?>"><?php echo $miesiac[$i]; ?></option>
				   <?php endfor; ?>
					</select>
										&nbsp;					&nbsp;
					<select id="customer_remuneration_deadline_1i" name="remuneration_deadline1i" class="short selectBox req">
						<option value="0">----</option>
					<?php for($i = date('Y'); $i <= date('Y')+6; $i++): ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				   <?php endfor; ?>
					</select>
				</div>

							<div class="form_row h_r_5">
									<label for="customer_current_employment_years">Okres zatrudnienia u obecnego pracodawcy</label>
										<span>Lata</span>
										<select id="customer_current_employment_years" name="current_employment_years" class="req short selectBox">
							<?php for($i = 0; $i <= 12; $i++): ?>
                        <option value="<?php echo $i; ?>"<?php if($i == '<ion:form:rejestracja:field:current_employment_years />'): ?> selected="selected"<?php endif ?>><?php echo $i; ?></option>
				   <?php endfor; ?>
				</select>
										<span>Miesiące</span>
										<select id="customer_current_employment_months" name="current_employment_months" class="req short selectBox">
											<?php for($i = 0; $i <= 12; $i++): ?>
		                               <option value="<?php echo $i; ?>"<?php if($i == '<ion:form:rejestracja:field:current_employment_months />'): ?> selected="selected"<?php endif ?>><?php echo $i; ?></option>
								   <?php endfor; ?>
				</select>
			</div>

							<div class="form_row h_r_6">
								<label for="customer_salary_to_personal_account">Czy wynagrodzenie jest przelewane na konto</label>
								<select id="customer_salary_to_personal_account" name="customer_salary_to_personal_account" class="short req selectBox">
									<option value="0">----</option>
									<option value="1">Tak</option>
									<option value="0">Nie</option>
								</select>
							</div>

					<!-- option 2 -->

					<div class="form_row h_r_7">
							<label for="customer_business_reqistration_date">Data rejestracji działalności</label>

		 								<select id="business_registration_date3i" name="business_registration_date3i" class="short selectBox req">
<option value="">--</option>
		 					<?php for($i = 1; $i <= 31; $i++): ?>
		                         <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		 				   <?php endfor; ?>
		 					</select>
		 					&nbsp;					&nbsp;
		 					<select id="business_registration_date2i" name="business_registration_date2i" class="medium selectBox req">
								<option value="">----</option>
		 					<?php
		 					$miesiac = array( '', 'styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień' );
		 					for($i = 1; $i <= 12; $i++): ?>
		                        <option value="<?php echo $i; ?>"><?php echo $miesiac[$i]; ?></option>
		 				   <?php endfor; ?>
		 					</select>
		 										&nbsp;					&nbsp;
		 					<select id="business_registration_date1i" name="business_registration_date1i" class="short selectBox req">
								<option value="">--</option>
							<?php for($i = date('Y'); $i >= date('Y')-80; $i--): ?>
                               <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						   <?php endfor; ?>
		 					</select>

					</div>

					<div class="form_row h_r_8">
								<label for="customer_university_name">Nazwa uczelni</label>
								<input id="customer_university_name" name="university_name" size="30" type="text" class="req">
					</div>

					<div class="form_row  h_r_9">
								<label for="customer_university_city">Miasto uczelni</label>
								<input id="customer_university_city" name="university_city" size="30" type="text" class="req">
					</div>

				</fieldset>
				<input type="hidden" name="form" value="dane">
                <button type="submit">Dalej</button>
            </form>
        </div>
        <div class="twoja_pozyczka">
        	<h3><strong>Twoja</strong> pożyczka</h3>
        	<div class="locked">
            	<fieldset class="kolo1">
                	<h5>Pożyczam</h5>
                    <div>
                    	<strong><?php if(isset($_SESSION['value'])): ?><?php echo $_SESSION['value'] ?><?php else: ?>250<?php endif ?></strong> zł
                        <input type="hidden" name="value" id="kwota" value="<?php if(isset($_SESSION['value'])): ?><?php echo $_SESSION['value'] ?><?php else: ?>600<?php endif ?>">
                    </div>
                </fieldset>
            	<fieldset class="kolo2">
                	<h5>Na czas</h5>
                    <div>
                    	<strong><?php if(isset($_SESSION['period'])): ?><?php echo $_SESSION['period'] ?><?php else: ?>7<?php endif ?></strong> dni
                        <input type="hidden" name="period" id="czas" value="<?php if(isset($_SESSION['period'])): ?><?php echo $_SESSION['period'] ?><?php else: ?>7<?php endif ?>">
                    </div>
                </fieldset>
            </div>
        </div>
	           <img src="<ion:theme_url />assets/images/pic.png" class="pic">

		   </section>

<ion:partial view="footer" />
