<ion:partial view="header_ty" />

<section class="page_wrapper centralize form full_width thankyou">
    	<h2>Potwierdzenie</h2>
        <div class="box">

        	<form>
            	<fieldset>
                    <div class="txt">
                    	<h4>Szanowny Kliencie, dziękujemy za wybór naszych usług.</h4>
						<p>Informujemy, że partnerem finansowym naszego serwisu jest portal<br/><a href="https://www.pozyczkomat.pl/" target="_blank"><img src="https://www.kobietazkasa.pl/themes/kobieta-z-klasa/assets/images/logo_small.png" width="125" height="20" alt="Pożyczkomat" style="vertical-align:middle;margin-top:10px"></a>.</p>
						<p>Wszelkie informacje o dalszych krokach otrzymasz za jego pośrednictwem.</p>
                        <p>Teraz należy zrobić tylko jeden mały krok i przelać 0,01 PLN ze swojego konta na nasze konto w jednym z poniższych banków a w temacie przelewu należy wpisać unikatowy kod weryfikacyjny, który otrzymaliście Państwo SMSem w trakcie procesu rejestracyjego.</p>
                    </div>
                	<div class="form_row">
                    	<label for="">Konto bankowe<br/>(wybierz swój bank)</label>
                        <select name="" class="req" id="bank" style="width:260px;">
							<option value="">----</option>
							<option value="8">Millennium Bank</option>
							<option value="9">Bank Zachodni WBK</option>
							<option value="10">PKO Bank Polski</option>
							<!-- <option value="11">Nordea Bank</option> -->
							<option value="12">Bank Pekao S.A.</option>
							<option value="13">mBank</option>
							<option value="14">Alior Bank</option>
							<option value="21">Inny bank</option>
						</select>
                    </div>
					<p><b><i>Jeżeli masz rachunek w innym Banku prosimy o dokonanie przelewu kwoty 0,01 PLN na nasz rachunek w Millennium Banku.</i></b></p>
					<div class="txt no-margin">
					<p><b>Płatność należy dokonać na nasz rachunek bankowy:</b></p>
					<p>FRIENDLY FINANCE Sp. z o. o.<br/>Złota 7/18, 00-019 Warszawa</p>
					<div class="bank bank8">
						<p>Nr konta:  84116022020000000235414218<br>
						<a href="http://www.bankmillennium.pl/" alt="Millennium Bank">http://www.bankmillennium.pl/</a><br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>

					<div class="bank bank9">
						<p>Nr konta:  72109017820000000120724363<br>
						<a href="http://www.bzwbk.pl/" alt="Bank Zachodni WBK">http://www.bzwbk.pl/</a><br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>

					<div class="bank bank10">
						<p>Nr konta:  54102010420000830202920619<br>
						<a href="http://www.pkobp.pl/" alt="PKO Bank Polski">http://www.pkobp.pl/</a><br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>

					<div class="bank bank11">
						<p>Nr konta:  54144013900000000015328576<br>
						<a href="http://www.nordea.pl/" alt="Nordea Bank">http://www.nordea.pl/</a><br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>

					<div class="bank bank12">
						<p>Nr konta:  55124010401111001051640915<br>
						<a href="http://www.pekao.com.pl/" alt="Bank Pekao S.A.">http://www.pekao.com.pl/</a><br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>

					<div class="bank bank13">
						<p>Nr konta:  98114010100000545351001001<br>
						<a href="http://www.mbank,pl/" alt="BRE Bank">http://www.mbank.pl/</a><br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>

					<div class="bank bank14">
						<p>Nr konta:  37249000050000452085895084<br>
						<a href="https://aliorbank.pl/" alt="Alior Bank">https://aliorbank.pl/</a><br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>		

					<div class="bank bank21">
						<p>Nr konta:  84116022020000000235414218<br><br>
						Prosimy nie dokonywać przelewu za pośrednictwem Blue Media S.A.</p>
					</div>
				</div>

                </fieldset>
            </form>

			<p style="text-align:center"><i>W przypadku pytań prosimy o kontakt pod numerem telefonu : +48 22 112 0 112.</i></p>

        </div>

	           <img src="<ion:theme_url />assets/images/pic.png" class="pic">

		   </section>

		   <?php

		   if('<ion:session:get key="customer_id" />' != ''): ?>


		   	<script type='text/javascript'>
var BehavioralMailingData = {
	partner: "novem",
	client: "kobietazkasa",
	pageType: "confirm",
	amount: '<ion:session:get key="value" />',
	productsIds: '<ion:session:get key="customer_id" />'
};
(function(){function e(){var e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('src','//api.behavioralmailing.com/js/data.js');if(document.getElementsByTagName('head').length>0){document.getElementsByTagName('head')[0].appendChild(e)}}if(document.readyState==='complete'){e()}else{if(window.addEventListener){window.addEventListener('load',function(){e()},false)}else{window.attachEvent('onload',function(){e()})}}})()
</script>


		   <?php if('<ion:session:get key="aff" />' == 'novem'): ?>

			   <iframe src="//ho.novem.pl/GLP3h?adv_sub=<ion:session:get key="customer_id" />&amount=<ion:session:get key="value" />" scrolling="no" frameborder="0" width="1" height="1"></iframe>

		   <?php elseif('<ion:session:get key="aff" />' == 'finmarket'): ?>

                           <!-- Offer Conversion: Kobieta z kasą -->
                            <img src="https://goaffiliate.go2cloud.org/aff_l?offer_id=16&adv_sub=<ion:session:get key="customer_id" />" width="1" height="1" />
                            <!-- // End Offer Conversion -->
       
			<?php elseif('<ion:session:get key="aff" />' == 'money'): ?>

				<img width="1" height="1" border="0" src="http://direct.money.pl/o/pozyczkomat/d/rd.html?mnyProdId=<ion:session:get key="prod_id" />&mnyAppId=<ion:session:get key="customer_id" />" />

			<?php elseif('<ion:session:get key="aff" />' == 'affiliate44'): ?>

				<img src="http://tracking.affiliate44.com/aff_l?offer_id=8&adv_sub=<ion:session:get key="customer_id" />" width="1" height="1" />

			<?php elseif('<ion:session:get key="aff" />' == 'sowafinansowa'): ?>

				<img src="https://www.google-analytics.com/collect?v=1&t=event&tid=UA-5659043-13&cid=<ion:session:get key="transaction_id" />&ec=offsite&ea=lead&el=KobietazKasa&ev=20&cd2=KobietazKasa&cd3=<ion:session:get key="transaction_id" />&cd4=lemonsky&cd5=<?php echo date('Y-m-d') . '%20' . date('H:i:s'); ?>&cd6=<ion:session:get key="customer_id" />&cd7=<ion:session:get key="transaction_id" />" />

			<?php elseif('<ion:session:get key="aff" />' == 'webepartners'): ?>

				<script type="text/javascript" src="https://webep1.com/lead/confirmation.js?mid=3279&refer=<ion:session:get key="customer_id" />"></script>

			<?php elseif('<ion:session:get key="aff" />' == 'pdm'): ?>

				<img src="https://9xwdi.voluumtrk2.com/conversion.gif?txid=<ion:session:get key="customer_id" />&payout=20" width="1" height="1"/>

			<?php elseif('<ion:session:get key="aff" />' == 'bankier'): ?>

				<img src="http://www.bankier.pl/e/signup/<ion:session:get key="bankier_id" />/<ion:session:get key="customer_id" />/" width="1" height="1" />

			<?php elseif('<ion:session:get key="aff" />' == 'novacash'): ?>

				<img src="https://program.novacash.eu/tracking/pixel_provision.gif?cpsid=cp-meh&hid=92bede0daaf171f3567fc9e39776accf&transaction_id=<ion:session:get key="customer_id" />" alt="" width="1" height="1" />
				<img src="https://program.novacash.eu/tracking/pixel_provision.gif?cpsid=cp-meo&hid=4598672e7aeb50e1dafda951fa0a0632&transaction_id=<ion:session:get key="customer_id" />" alt="" width="1" height="1" />

			<?php elseif('<ion:session:get key="aff" />' == 'doaff'): ?>

				<script type="text/javascript">
				    document.write(unescape("%3Cscript id='doaffiliate' src='" + (("https:" == document.location.protocol) ? "https://" : "http://")
				    + "tracker2.doaffiliate.net/js/doaffsale.js' type='text/javascript'%3E%3C/script%3E"));
				</script>
				<script type="text/javascript">
				    DoAffTracker.setMerchantId(168);
				    DoAffTracker.setCampaignId(181);
				    DoAffTracker.setActionId(290);
				    DoAffTracker.setOrderId('<ion:session:get key="customer_id" />');
				    DoAffTracker.setPublicKey('<?php echo md5("168_181_290_<ion:session:get key="customer_id" />_<ion:session:get key="value" />_".time());?>');
				    DoAffTracker.setChecksum('<?php echo md5(md5("168_181_290_<ion:session:get key="customer_id" />_<ion:session:get key="value" />_".time())."W~h3nyG*R2|;|DY-_VuZ}iXp,b?/T>re5BvQI<0oLk)1EN#j4`$:sCSfFt9z[(H6");?>');
				    try {DoAffTracker.trackSale();}
				    catch (err) { }
				</script>

			<?php elseif('<ion:session:get key="aff" />' == 'omg'): ?>

				<script src="https://track.omgpl.com/673678/transaction.asp?APPID=<ion:session:get key="customer_id" />&MID=673678&PID=13467&status="></script>
				<noscript><img src="https://track.omgpl.com/apptag.asp?APPID=<ion:session:get key="customer_id" />&MID=673678&PID=13467&status=" border="0" height="1" width="1"></noscript>

			<?php elseif('<ion:session:get key="aff" />' == 'APCIteratorzy'): ?>

				<iframe src="https://ssl.affizzy.com/aff_l?offer_id=136&adv_sub=<ion:session:get key="customer_id" />" scrolling="no" frameborder="0" width="1" height="1"></iframe>

			<?php elseif('<ion:session:get key="aff" />' == 'leadiance'): ?>	

				<img src="http://eu.afftrack.net/?k=l-43-9f64-48-a294&ref=<ion:session:get key="customer_email" />" width="1" height="1" border="0" />

			<?php elseif('<ion:session:get key="aff" />' == 's4ad'): ?>

				<img src="http://www.solutions4ad.com/partner/scripts/sale.php?AccountId=402fac2e&TotalCost=0&OrderID=<ion:session:get key="customer_id" />&ActionCode=KOBIETAZKASASALE&CampaignID=bfa96cd7" width="1" height="1">

			<?php elseif('<ion:session:get key="aff" />' == 'inis'): ?>

				<script type='text/javascript'>
					var valueCart = '<ion:session:get key="value" />';
					var leadId = '<ion:session:get key="customer_id" />';
				</script>
				<script type='text/javascript'>
					var OA_p = (location.protocol=='https:'?'https://inistrack.net/d/tjs.php':'http://inistrack.net/d/tjs.php');
					var OA_r=Math.floor(Math.random()*999999);
					document.write ("<" + "script language='JavaScript' ");
					document.write ("type='text/javascript' src='"+OA_p);
					document.write ("?trackerid=1716&append=0&r="+OA_r+"'><" + "\/script>");
					</script>
					<noscript><div id='m3_tracker_1716' style='position: absolute; left: 0px; top: 0px; visibility: hidden;'><img src='http://inistrack.net/d/ti.php?trackerid=1716&valueCart=<ion:session:get key="value" />&leadId=<ion:session:get key="value" />&cb=<?php echo rand(0,999999) ?>' width='0' height='0' alt='' /></div></noscript>

				<?php elseif('<ion:session:get key="aff" />' == 'ebroker'): ?>

				<script type="text/javascript" src="//tracking.leadaff.pl/la_track.js?PID=73&AID=999&OID=<ion:session:get key="customer_id" />"></script>

				<img src="https://afilea.go2cloud.org/SLrn?adv_sub=" width="1" height="1" />

				<?php elseif('<ion:session:get key="aff" />' == 'tradetracker'): ?>

				<img src="https://tl.tradetracker.net/?cid=16544&pid=24664&tid=<ion:session:get key="customer_id" />&descrMerchant=&descrAffiliate=" alt="" width="1" height="1" border="0"  />
                              
                                <?php elseif('<ion:session:get key="aff" />' == 'adtraction'): ?>
                                     
                                    <!-- Adtraction tracking code - do not remove -->
                                    <script type="text/javascript" src="https://track.adtraction.com/t/t?t=4&tk=1&am=0&c=PLN&ti=<ion:session:get key="customer_id" />&tp=1080082401&trt=2&ap=1080081680" charset="ISO-8859-1"></script>
                                    
                                    
                                <?php elseif('<ion:session:get key="aff" />' == 'convertiser'): ?>
                                    
                                    <iframe src="//converti.se/conversion/3394c616-c7b9e687-1c3fde8471c7/?tracking_id=<ion:session:get key="customer_id" />" scrolling="no" frameborder="0" width="1" height="1"></iframe>

                                   
                    <?php endif; endif; ?>

	<!-- (c) 2000-2014 AdOcean Sp. z o.o. version 2.0 / pozyczkomat_lead -->
<script type="text/javascript"><!--
_afiact_xoqmpnffsf = new Image(1,1);
_afiact_xoqmpnffsf.src='http://interiaafi.hit.gemius.pl/_'+(new Date()).getTime()+'/redot.gif?id=d2CVJscykY0vH99X.KM2tfWpfSTp4e_uUfAjngf3tCD.M7';
//--></script>

<!-- Google Code for Wyslany wniosek Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 962812905;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "fMluCITP31YQ6beNywM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/962812905/?label=fMluCITP31YQ6beNywM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<ion:partial view="footer" />
