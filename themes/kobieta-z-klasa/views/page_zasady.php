<ion:partial view="header" />

<section class="page_wrapper centralize zasady">
        <ion:page:articles type="">

			<ion:article>
                <ion:title tag="h2" />
                <ion:content />
			</ion:article>

        </ion:page:articles>
		
		<ion:partial view="calculator" />	
		
    </section>

<ion:partial view="footer" />
