<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="ie7"> <![endif]-->
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><ion:meta_title /> <ion:site_title /></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <meta name="description" content="<ion:meta_description />">

    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="stylesheet" href="<ion:theme_url />assets/test_css/core.css" />
    <link rel="stylesheet" href="<ion:theme_url />assets/test_css/style.css" />

    <script src="<ion:theme_url />assets/js/plugins/jquery-1.9.1.min.js"></script>
    <script src="<ion:theme_url />assets/js/plugins/jquery.selectBox.js"></script>
    <script src="<ion:theme_url />assets/js/plugins/jquery.bxslider.js"></script>

    <script src="<ion:theme_url />assets/js/init.js"></script>

    <!--[if IE]>
    <script src="code/js/plugins/placeholder.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="code/js/libs/html5shiv.js"></script>
    <![endif]-->


         <script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

   ga('create', 'UA-43038387-3', 'auto');
   ga('send', 'pageview');

 </script>
</head>
<body class="home">
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TXN8ZN"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TXN8ZN');</script>
    <!-- End Google Tag Manager -->
	<header class="centralize clearfix">
    	<h1><a href="/"><img src="https://www.kobietazkasa.pl/themes/kobieta-z-klasa/assets/images/logo.png" width="244" height="140"></a></h1>
        <nav>
			<ul>
				<li class="active">
					<a href="https://www.kobietazkasa.pl/">Home</a>
				</li>

				<li>
					<a href="https://www.kobietazkasa.pl/zasady">Zasady</a>
				</li>

				<li>
					<a href="https://www.kobietazkasa.pl//faq">FAQ</a>
				</li>

				<li>
					<a href="https://www.kobietazkasa.pl//stylebook">Stylebook</a>
				</li>

				<li>
					<a href="https://www.kobietazkasa.pl//kontakt">Kontakt</a>
				</li>
			</ul>
        </nav>
        <p class="kontakt">Kontakt z Konsultantką <strong>22 388 32 10</strong></p>
    <!--    <p class="partner">Partner serwisu Pożyczkomat.pl</p> -->
    </header>


<section class="page_wrapper home">

	<div class="hero">



				                    <h2><strong>Pożyczki</strong></h2><br/>
                                    <span class="title"> grzechu warte! </span>


    	<p class="txt1 justify">Gwarancja poufności i bezpieczeństwo. <br />Pieniądze <strong>otrzymasz w 10 minut</strong> na konto.</p>

    	<form method="post" action="">
        	<fieldset class="kolo1">
            	<h5>Pożyczam</h5>
                    <div>
                    	<strong>750</strong> zł
                        <input type="hidden" name="value" id="kwota" value="750">
                    	<a class="plus">plus</a>
                    	<a class="minus">minus</a>
                    </div>
                    <p>(100-1 500 zł)</p>
            </fieldset>
        	<fieldset class="kolo2">
                	<h5>Na czas</h5>
                    <div>
                    	<strong>27</strong> dni
                        <input type="hidden" name="period" id="czas" value="27">
                    	<a class="plus">plus</a>
                    	<a class="minus">minus</a>
                </div>
                <p>(7-31 dni)</p>
            </fieldset>
        	<fieldset class="kolo3">
            	<p class="t1">Spłata do <span class="to"></span> <br>Pożyczka <span class="l_value"></span> zł</p>
				<input type="hidden" name="form" value="kalkulator"/>
                <button type="submit"><strong>Weź</strong> pożyczkę</button>
                <p class="t2">Prowizja <span class="fee"></span> zł <br>Razem <span class="total"></span> zł</p>
                <p class="t3">RRSO <span class="rrso"></span>%</p>
            </fieldset>
        </form>
    </div>

    	<div class="gwarancja">




						                    <h2><strong>Gwarancja</strong> <br />bezpieczeństwa</h2>
<ul>
<li class="first">
<h3>Uczciwe zasady</h3>
<div>
<p><strong>Wszystko jest jasne</strong> – nasza oferta nie zawiera żadnych ukrytych opłat. Jest skonstruowana w przejrzysty i praktyczny sposób, by nie przysparzała Ci dodatkowych problemów. Wiemy, że bycie fair się opłaca!</p>
<div class="bg"></div>
</div>
</li>
<li class="second">
<h3>Szybki przelew</h3>
<div>
<p><strong>Liczy się czas</strong>. Przelew środków otrzymasz zaraz po zweryfikowaniu Twojego wniosku. A decyzję o weryfikacji otrzymasz nawet w 10 minut – jesteśmy dla Ciebie dostępni 24 godziny na dobę.</p>
<div class="bg"></div>
</div>
</li>
<li class="third">
<h3>Elastyczna spłata</h3>
<div>
<p><strong>Dbamy o Twój komfort</strong> – zaciągniętą pożyczkę możesz spłacić za pośrednictwem bankowości internetowej lub w tradycyjnej placówce bankowej. A jeśli z jakiejś przyczyny masz problemy ze spłatą – możesz przedłużyć termin uregulowania płatności.</p>
<div class="bg"></div>
</div>
</li>
</ul>





        </div>
    	<div class="opinia">




						                    <h5>office manager</h5>
<h4>Ania, 23 lat</h4>
<h3>Często wypadam po pracy<br />na małe zakupy</h3>
<p class="txt">Moją wielką pasją jest moda, a nowy ciuch potrafi rozjaśnić nawet najgorszy dzień. Uwielbiam tworzyć nowe stylizacje. Czasem zdarza się, że wystrzałowa sukienka (i to akurat w moim rozmiarze!) wpadnie mi w oko tuż przed wypłatą – a przecież nie można przepuścić takiej okazji. Dzięki szybkiej pożyczce mogę sobie na nią pozwolić i nie bać się, że gdy z pensją w portfelu wrócę do sklepu, mojej wymarzonej sukienki dawno już nie będzie.</p>
<p><img src="https://www.kobietazkasa.pl//files/home/girl.png" /></p>





        </div>
    </section>


<footer>
    	<a href="mailto:kontakt@kobietazkasa.pl">kontakt@kobietazkasa.pl</a>
        <p class="kontakt">Kontakt z Konsultantką <br><strong>+48 22 388 32 10</strong></p>
		<ul>
			<li class="active">
				<a href="https://www.kobietazkasa.pl/">Home</a>
			</li>

			<li>
				<a href="https://www.kobietazkasa.pl/zasady">Zasady</a>
			</li>

			<li>
				<a href="https://www.kobietazkasa.pl/faq">FAQ</a>
			</li>

			<li>
				<a href="https://www.kobietazkasa.pl/stylebook">Stylebook</a>
			</li>

			<li>
				<a href="https://www.kobietazkasa.pl/kontakt">Kontakt</a>
			</li>
		</ul>
        <p>Wszelkie Prawa zastrzeżone 2014 | Kobietazkasa.pl</p>
        <!--<p>Friendly Finance Poland spółka z ograniczoną odpowiedzialnością z siedzibą w Warszawie 00-019 przy ul. Złotej 7/18, telefon +48 22 388 32 10. NIP: 1070025046, REGON: 146512445. Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego KRS: 0000448749. Kapitał zakładowy w wysokości 1.000 000,00 PLN</p>
        --><div>
            <p>Jednorazowa opłata rejestracyjna wynosi 0,01 PLN. Reprezentatywny przykład: całkowita kwota pożyczki 500 PLN; czas obowiązywania umowy 31 dni; oprocentowanie 0%; opłata przygotowawcza 0%; prowizja 134 PLN; całkowita kwota do zapłaty 634 PLN; RRSO 1537.4% (stan na dzień 12.12.2013r.).</p>
            <p>Dodatkowe koszty związane z pożyczką mogą być naliczone wyłącznie w momencie braku terminowej spłaty udzielonej pożyczki. W przypadku opóźnienia w spłacie Pożyczki, Pożyczkodawca zastrzega sobie możliwość naliczenia odsetek za zwłokę w wysokości czterokrotnej wysokości stopy kredytu lombardowego Narodowego Banku Polskiego oraz podjęcia działań mających na celu zwrot Pożyczki przez Pożyczkobiorcę. W szczególności Pożyczkodawca wezwie Pożyczkobiorcę do zapłaty telefonicznie lub wysyłając SMS, e-mail lub pisemne wezwanie do zapłaty. Pożyczkobiorca zostanie obciążony kosztami tych monitów zgodnie z warunkami obowiązującej Tabeli Opłat i Prowizji. Kosztami dochodzenia zwrotu Pożyczki na drodze postępowania sądowego zostanie obciążony Pożyczkobiorca.</p>
            <p>W przypadku łącznego spełnienia przesłanek określonych w artykule 14 lub 16 bądź 17 ustawy z dnia 9 kwietnia 2010 r. o udostępnianiu informacji gospodarczych i wymianie danych gospodarczych (Dz.U. Nr 81, poz. 530) Pożyczkodawca ma prawo przekazać informacje o zobowiązaniach Pożyczkobiorcy do Biur Informacji Gospodarczej, co może mieć wpływ na zdolność kredytową Pożyczkobiorcy. Przekazanie danych Pożyczkobiorcy do Biur Informacji Gospodarczej jest dokonywane na podstawie pisemnej umowy o udostępnianie informacji gospodarczych zawartej przez Pożyczkodawcę z Biurem Informacji Gospodarczej oraz w oparciu o przepisy rozdziału 3 ustawy o udostępnianiu informacji gospodarczych i wymianie danych gospodarczych.</p>
            <p>Przedłużenie spłaty pożyczki nie odbywa się automatycznie i jest zależne od woli klienta. Przedłużenie terminu spłaty odbywa się po dokonaniu opłaty za przedłużenie, której wysokość zależy od kwoty pożyczki i terminu przedłużenia. </p>
        </div>
    </footer>

    <script type='text/javascript'>
     document.MAX_ct0 = unescape('');

     var m3_u = (location.protocol=='https:'?'https://inistrack.net/d/ajs.php':'http://inistrack.net/d/ajs.php');
     var m3_r = Math.floor(Math.random()*99999999999);
     if (!document.MAX_used) document.MAX_used = ',';
     document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
     document.write ("?zoneid=10977");
     document.write ('&amp;cb=' + m3_r);
     if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
     document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
     document.write ("&amp;loc=" + escape(window.location));
     if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
     if (document.context) document.write ("&context=" + escape(document.context));
     if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
         document.write ("&amp;ct0=" + escape(document.MAX_ct0));
     }
     if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
     document.write ("'><\/scr"+"ipt>");
  </script>
  <noscript><a href='http://inistrack.net/d/ck.php?n=afc91cdf' target='_blank'><img src='http://inistrack.net/d/avw.php?zoneid=10977' border='0' alt='' /></a></noscript>

<script type="text/javascript" >
var reachlocalTRKDOM="rtsys.rtrk.pl";
(function() {
var rlocal_load = document.createElement("script");
rlocal_load.type = "text/javascript";
rlocal_load.src = document.location.protocol+"//"+reachlocalTRKDOM+"/rct_lct/js/rlrct1.js";
(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild (rlocal_load);
})(); </script>


</body>
</html>
